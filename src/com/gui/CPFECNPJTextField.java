package com.gui;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public class CPFECNPJTextField extends TextField
{

    public CPFECNPJTextField()
    {
        this.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) ->
        {
            String value = getText();
            if (!newValue)
            {
                if (getText().length() == 11)
                {
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})$", "$1.$2.$3-$4");
                }
                if (getText().length() == 14)
                {
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})$", "$1.$2.$3/$4-$5");
                }
            }
            setText(value);
            if (!getText().equals(value))
            {
                setText("");
                insertText(0, value);
            }
        });

        positionCaret();
        maxField(18);
    }

    private void positionCaret()
    {
        Platform.runLater(() ->
        {
            positionCaret(getText().length());
        });
    }

    /**
     * @param textField TextField.
     * @param length Tamanho do campo.
     */
    private void maxField(final Integer length)
    {
        textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) ->
        {
            if (newValue.length() > length)
            {
                setText(oldValue);
            }
        });
    }
}
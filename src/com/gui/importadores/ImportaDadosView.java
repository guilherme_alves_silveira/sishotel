package com.gui.importadores;

import com.gui.importadores.interfaces.ClienteDoImportador;
import com.gui.template.TemplatePaginator;
import com.gui.util.AlertaExceptionUtil;
import com.model.base.Entidade;
import java.util.Objects;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Classe que deve ser utilizada com o cliente do importar para que tenha
 * utilizada a sua utilização.
 *
 * @author Guilherme A. Silveira
 * @param <T> Tipo da classe
 */
public abstract class ImportaDadosView<T extends Entidade> extends TemplatePaginator<T>
{
    private ClienteDoImportador clienteDoImportador;
    private Button btnImportar;

    public ImportaDadosView(String titulo, String[] nomeDasColunas,
                            String[] nomePropertys, Integer[] minWidth)
    {
        super(titulo, nomeDasColunas, nomePropertys, minWidth);
        inicia();
    }

    public ImportaDadosView(String titulo, String[] nomeDasColunas,
                            String[] nomePropertys, Integer[] minWidth, Integer numCols)
    {
        super(titulo, nomeDasColunas, nomePropertys, minWidth, numCols);
        inicia();
    }

    @Override
    protected void inicia()
    {
        iniciaComboBoxDeQuantidade();
        iniciaCriacaoDeTableView();
        iniciaPagination();
        iniciaFieldEButtonGerais();
        iniciaFieldImportacao();
        iniciaLayoutGeral();
        iniciaEventos();
        iniciaEventoImportador();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    private void iniciaFieldImportacao()
    {
        Image imgImporta = new Image(getClass().getResourceAsStream("/com/imgs/icone_importa.png"));
        this.btnImportar = new Button("", new ImageView(imgImporta));
        this.btnImportar.setTooltip(new Tooltip("Clique neste botão para importar os dados, mas antes selecione o item na grade."));
        this.getHbPesquisa().getChildren().add(btnImportar);
    }

    private void iniciaEventoImportador()
    {
        AESetItemNoClienteDoImportador();
    }

    public void setClienteDoImportador(ClienteDoImportador clienteDoImportador)
    {
        if (clienteDoImportador != null)
        {
            this.clienteDoImportador = clienteDoImportador;
        }
        else
        {
            throw new IllegalArgumentException("Insirá um cliente de importador válido não nulo.");
        }
    }

    /**
     * AE = ActionEvent. Função utilizada para passar o item pego na tabela,
     * para o cliente do importar que vai utilizar o entidade passada.
     */
    protected void AESetItemNoClienteDoImportador()
    {
        btnImportar.setOnAction((e) ->
        {
            setItemSelecionado();
        });
    }

    protected Button getBtnImportar()
    {
        return btnImportar;
    }

    private void setItemSelecionado()
    {
        try
        {
            Objects.requireNonNull(clienteDoImportador, "Configure o importador primeiro com o setClienteDoImportador para utilizar este método.");

            T item = getTbDados().getSelectionModel().getSelectedItem();
            if (item != null)
            {
                clienteDoImportador.setItem(item);
                this.getWindow().close();
            }
            else
            {
                Alert alerta = new Alert(Alert.AlertType.WARNING);
                alerta.setHeaderText("Atenção!");
                alerta.setContentText("Selecione por gentileza um item válido para realizar a importação.");
                alerta.showAndWait();
            }
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }
}
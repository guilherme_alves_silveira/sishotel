package com.gui.importadores;

import com.model.dao.GeralUtilDAO;
import com.model.po.Pais;
import java.util.List;
import javafx.scene.control.Tooltip;

public class ImportaPaisesView extends ImportaDadosView<Pais>
{
    private final static String estiloToolTip = "-fx-font-size: 12pt;";

    private final static String[] nomeDasColunas =
    {
        "ID", "Nome", "Código"
    };
    private final static String[] nomePropertys =
    {
        "id", "nome", "codigo"
    };
    private final static Integer[] minWidth =
    {
        0,
        185,
        100
    };

    public ImportaPaisesView()
    {
        super("Selecione o Pais", nomeDasColunas, nomePropertys, minWidth, 3);
        adicionaTooltip();
    }

    @Override
    protected List<Pais> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        return GeralUtilDAO.buscaPaginadaDePaises(primeiroResultado, quantidadeDeResultados);
    }

    @Override
    protected List<Pais> buscaListaFiltrada(Pais entidade, String... propriedades)
    {
        if ((propriedades != null && propriedades.length > 0))
        {
            return GeralUtilDAO.buscaListaFiltradaDePaises(entidade, propriedades);
        }

        return getListaDeDados();
    }

    private void adicionaTooltip()
    {
        Tooltip tpiTxtPesquisa = new Tooltip("Digite o código ou o nome.");
        Tooltip tpiBtnPesquisa = new Tooltip("Clique no botão com a lupa para realizar a pesquisa.");
        tpiTxtPesquisa.setStyle(estiloToolTip);
        tpiBtnPesquisa.setStyle(estiloToolTip);
        this.getTxtPesquisa().setTooltip(tpiTxtPesquisa);
        this.getBtnPesquisa().setTooltip(tpiBtnPesquisa);
    }

    @Override
    protected void AERealizaPesquisaComCriterios()
    {
        this.getBtnPesquisa().setOnAction((e) ->
        {
            String busca = getTxtPesquisa().getText().trim();
            if (!busca.equals(""))
            {
                String criterios[] = busca.split("\\s+");

                if (criterios.length > 0)
                {
                    Pais entidade = getFiltroPais(criterios);
                    setNovosDados(buscaListaFiltrada(entidade, nomePropertys));
                }
            }
        });
    }

    /**
     * 
     * @param criterios Os critérios são os valores colocados dentro da entidade,
     * pois o mesmo válida se as informações passadas, são compatíveis 
     * com os atributos da entidade.
     * @param busca O valor passado, que será utilizado para pegar os critérios.
     * @return 
     */
    private Pais getFiltroPais(String[] criterios)
    {
        Pais entidade = new Pais();
        for (String criterio : criterios)
        {
            if (criterio.matches("[0-9]+"))
            {
                entidade.setCodigo(criterio);
            }
            else
            {
                entidade.setNome(criterio);
            }
        }
        return entidade;
    }

    /**
     * @return O total de itens para configurar a paginação.
     */
    @Override
    protected Long buscaTotalDeItens()
    {
        return GeralUtilDAO.buscaTotalDePais();
    }
}
package com.gui.importadores.interfaces;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import jfxtras.labs.scene.control.window.Window;

public interface View<T>
{
    public void permiteEdicao();

    public void permiteApenasVisualizacao();

    public void setEntidade(T entidade);

    public Window getWindow();

    /**
     * Função utilizada para habilitar os nós
     * @param no Pode ser TextField, ComboBox, DatePicker ou Button
     */
    default public void habilitaCamposBotoes(Node no)
    {
        if (no instanceof TextInputControl)
        {
            TextInputControl desbloqueado = (TextInputControl) no;
            desbloqueado.setEditable(true);
        }
        else if (no instanceof ComboBoxBase)
        {
            ComboBoxBase desbloqueado = (ComboBoxBase) no;
            desbloqueado.setDisable(false);
        }
        else if (no instanceof Button)
        {
            Button desbloquado = (Button) no;
            desbloquado.setDisable(false);
        }
    }

    /**
     * Função utilizada para desabilitar os nós
     * @param no Pode ser TextField, ComboBox, DatePicker ou Button
     */
    default public void desabilitaCamposBotoes(Node no)
    {
        if (no instanceof TextInputControl)
        {
            TextInputControl bloqueado = (TextInputControl) no;
            bloqueado.setEditable(false);
        }
        else if (no instanceof ComboBoxBase)
        {
            ComboBoxBase bloqueado = (ComboBoxBase) no;
            bloqueado.setDisable(true);
        }
        else if (no instanceof Button)
        {
            Button desbloquado = (Button) no;
            desbloquado.setDisable(true);
        }
    }
    
    default public void limpaCampos(Node no)
    {
        if (no instanceof TextField)
        {
            TextField aLimpar = (TextField) no;
            aLimpar.setText("");
        }
    }
}
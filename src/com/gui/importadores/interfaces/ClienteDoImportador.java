package com.gui.importadores.interfaces;

/**
 * Interface utilizada para passar a entidade do importador
 * para a classe que implementa está interface.
 * @param <T> Tipo da entidade\objeto passado.
 */
public interface ClienteDoImportador<T>
{
    public void setItem(T entidade);
    public void configuraImportador();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui.template;

import com.controller.abstracts.InitializableImpl;

/**
 *
 * @author A-IKASORUK
 */
public abstract class TemplateView extends InitializableImpl
{

    /**
     * Método que é utilizado para reiniciar a tela ao padrão, que é colocar o
     * ultimo id + 1 do banco de dados no campo de id.
     */
    protected abstract void resetaTela();

    /**
     * @return Retorna o ultimo id do banco de dados mais 1.
     */
    protected abstract Long getUltimoNumeroDeEntidade();

    /**
     * Utilizado ao iniciar a tela ou quando o foco é perdido do campo id.
     */
    protected abstract void setUltimoNumero();
}

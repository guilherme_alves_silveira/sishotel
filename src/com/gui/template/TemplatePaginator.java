package com.gui.template;

import com.controller.abstracts.InitializableImpl;
import com.model.base.Entidade;
import java.util.List;
import java.util.Objects;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public abstract class TemplatePaginator<T extends Entidade> extends InitializableImpl
{
    private final String[] nomeDasColunas;
    private final String[] nomePropertys;
    private final Integer[] minWidth;
    private final static String estiloFonteGeral = "-fx-font-size: 11pt;";
    private int numCols = 2;
    private final Label lbTitulo;
    private final BorderPane bpLayoutGeral = new BorderPane();
    private final ObservableList<T> dados = FXCollections.observableArrayList();
    private ComboBox<Integer> cbQtdItensPorPagina;
    private HBox hbPesquisa;
    private TextField txtPesquisa;
    private Button btnPesquisa;
    private Pagination pgDados;
    private TableView<T> tbDados;

    public TemplatePaginator(String titulo, String[] nomeDasColunas,
                             String[] nomePropertys, Integer[] minWidth)
    {
        super();
        verificaSeENuloOuVazio(nomeDasColunas, nomePropertys, minWidth);
        this.nomeDasColunas = nomeDasColunas;
        this.nomePropertys = nomePropertys;
        this.minWidth = minWidth;
        this.lbTitulo = new Label(titulo);
        getWindow().setTitle(titulo);
    }

    public TemplatePaginator(String titulo, String[] nomeDasColunas,
                             String[] nomePropertys, Integer[] minWidth, Integer numCols)
    {
        this(titulo, nomeDasColunas, nomePropertys, minWidth);
        this.setNumCols(numCols);
    }

    protected void iniciaComboBoxDeQuantidade()
    {
        this.cbQtdItensPorPagina = new ComboBox<>();
        this.cbQtdItensPorPagina.setVisibleRowCount(3);
        for (int i = 5; i <= 30; i += 5)
        {
            cbQtdItensPorPagina.getItems().add(i);
        }
    }

    protected void iniciaFieldEButtonGerais()
    {
        Image imgBusca = new Image(getClass().getResourceAsStream("/com/imgs/icone_busca.png"));
        this.btnPesquisa = new Button("", new ImageView(imgBusca));
        this.btnPesquisa.setTooltip(new Tooltip("Clique neste botao para realizar a pesquisa."));
        this.txtPesquisa = new TextField();
        this.hbPesquisa = new HBox();
        this.hbPesquisa.setSpacing(10.0);
        this.hbPesquisa.setAlignment(Pos.CENTER);
        this.hbPesquisa.setStyle(estiloFonteGeral);
        this.hbPesquisa.getChildren().addAll(cbQtdItensPorPagina,
                                             txtPesquisa,
                                             btnPesquisa);
    }

    protected void iniciaEventos()
    {
        CLAtualizadaPaginacaoAoMudarQuantidadeNoCb();
        AERealizaPesquisaComCriterios();
    }

    /**
     * Inicia a paginação
     */
    protected final void iniciaPagination()
    {
        this.pgDados = new Pagination();
        this.pgDados.setStyle(estiloFonteGeral);
        setNumeroTotalDePaginas();
        pgDados.setPageFactory((Integer pageIndex) -> criaTableViewPaginado(pageIndex));
    }

    protected final void setNumeroTotalDePaginas() throws NumberFormatException
    {
        Double numeroTotalDeItens = buscaTotalDeItens().doubleValue() / getItensPorPagina();
        numeroTotalDeItens = Math.ceil(numeroTotalDeItens);
        if (numeroTotalDeItens < 1.0)
        {
            numeroTotalDeItens = 1.0;
        }
        pgDados.setPageCount(Integer.parseUnsignedInt(String.valueOf((int)numeroTotalDeItens.intValue())));
    }

    protected final TableView<T> criaTableViewPaginado(int index)
    {
        int posicaoDosProximosItens = index * getItensPorPagina();
        dados.clear();
        dados.setAll(buscaPaginadaDeDados(posicaoDosProximosItens, getItensPorPagina()));
        tbDados.setItems(dados);
        return tbDados;
    }

    protected final void iniciaCriacaoDeTableView()
    {
        tbDados = new TableView<>();
        for (int i = 0; i < numCols; i++)
        {
            TableColumn col = new TableColumn(nomeDasColunas[i]);
            col.setMinWidth(minWidth[i]);
            col.setCellValueFactory(new PropertyValueFactory<>(nomePropertys[i]));
            tbDados.getColumns().add(col);
        }
        tbDados.setStyle(estiloFonteGeral);
    }

    protected void iniciaLayoutGeral()
    {
        VBox posicionaNoCentro = new VBox(lbTitulo);
        posicionaNoCentro.setAlignment(Pos.CENTER);
        bpLayoutGeral.setTop(posicionaNoCentro);
        bpLayoutGeral.setCenter(pgDados);
        bpLayoutGeral.setBottom(hbPesquisa);
        bpLayoutGeral.setPadding(new Insets(10));
        bpLayoutGeral.setPrefSize(500, 300);
    }

    protected Integer getItensPorPagina()
    {
        Integer numeroDeItensNaPagina = cbQtdItensPorPagina.selectionModelProperty()
                .getValue()
                .getSelectedItem();
        return numeroDeItensNaPagina != null ? numeroDeItensNaPagina : 5;
    }

    /**
     * AE = ActionEvent. Função que implementa o btnPesquisa. O mesmo deve ser
     * implementado pelas classes filhas de ImportaDadosView, pois os properties
     * das classes filhas podem variar. Obs.: Os properties representam os
     * getters da entidade, como por exemplo getCodigo, getNome, que utilizando
     * os properties ficaria codigo, nome, removendo o get do começo.
     */
    protected abstract void AERealizaPesquisaComCriterios();

    /**
     * CL = ChangeListener. Está função atualiza a tabela, para refazer a
     * paginação de acordo com a quantidade definida no combo-box.
     */
    protected void CLAtualizadaPaginacaoAoMudarQuantidadeNoCb()
    {
        this.cbQtdItensPorPagina.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) ->
        {
            /**
             * Se o valor antigo, for diferente do novo, executa a função.
             */
            if (!Objects.equals(oldValue, newValue))
            {
                setNumeroTotalDePaginas();
            }
        });
    }

    protected BorderPane getBpLayoutGeral()
    {
        return bpLayoutGeral;
    }

    protected TextField getTxtPesquisa()
    {
        return txtPesquisa;
    }

    protected Button getBtnPesquisa()
    {
        return btnPesquisa;
    }

    protected void setNovosDados(List<T> entidade)
    {
        dados.clear();
        dados.setAll(entidade);
    }

    protected List<T> getListaDeDados()
    {
        return dados;
    }
    
    protected HBox getHbPesquisa()
    {
        return hbPesquisa;
    }
    
    protected TableView<T> getTbDados()
    {
        return tbDados;
    }

    /**
     * Método a ser implementado que vai retornar os dados necessários,
     * utilizados para a paginação.
     *
     * @param primeiroResultado A partir do primeiro resultado.
     * @param quantidadeDeResultados Quantidade máxima de resultados a ser
     * buscado
     *
     * @return Dadoas retornados
     */
    protected abstract List<T> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados);

    /**
     * @return Total de Itens
     */
    protected abstract Long buscaTotalDeItens();

    /**
     * @param entidade Entidade a ser passada
     * @param propriedades Os nomes das propriedades (getters) que serão
     * utilizados na pesquisa
     *
     * @return Retorna uma lista de resultados encontrados que atenderam aos
     * critérios.
     */
    protected abstract List<T> buscaListaFiltrada(T entidade, String... propriedades);

    private void setNumCols(int numCols)
    {
        if (numCols < 1)
        {
            throw new IllegalArgumentException("O número de colunas não pode ser menor que 1.");
        }
        this.numCols = numCols;
    }

    protected void setBpLayoutGeralSize(int width, int height)
    {
        if (width < 1 || height < 1)
        {
            throw new IllegalArgumentException("O tamanho não pode ser menor que 1.");
        }
        bpLayoutGeral.setPrefSize(width, height);
    }

    protected void verificaSeENuloOuVazio(Object[]... objetos) throws IllegalArgumentException
    {
        for (Object[] obj : objetos)
        {
            Objects.requireNonNull(obj, "O objeto passado não pode ser nulo");
            if (obj.length <= 0)
            {
                throw new IllegalArgumentException("O array passado não pode ficar vázio.");
            }
        }
    }
}
package com.gui.template;

import com.controller.abstracts.Controller;
import com.gui.CPFECNPJTextField;
import com.gui.importadores.ImportaPaisesView;
import com.gui.importadores.interfaces.ClienteDoImportador;
import com.gui.importadores.interfaces.View;
import com.gui.util.AlertaExceptionUtil;
import com.gui.util.FormattedTextFieldUtil;
import com.gui.util.LayoutManagerUtil;
import com.model.dao.GeralUtilDAO;
import com.model.po.Cidade;
import com.model.po.Estado;
import com.model.po.Pais;
import com.model.po.abstracts.Endereco;
import com.model.po.abstracts.Pessoa;
import com.model.po.enumerated.Sexo;
import com.model.vo.util.TempoUtil;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import jfxtras.labs.scene.control.window.Window;

public abstract class TemplataPessoa<T extends Pessoa, E extends Endereco, C extends Controller> extends TemplateView
        implements ClienteDoImportador<Pais>, View<T>
{

    private ImportaPaisesView importador = new ImportaPaisesView();
    // As entidades pessoa e endereço serão utilizados para o CRUD.
    protected T pessoa;
    protected E endereco;
    protected C controller;
    protected Pais paisImportado;
    /* Cadastros Básicos de Pessoa */
    private Label lbId;
    private Label lbNome;
    private Label lbCFP;
    private Label lbRG;
    private Label lbPassaporte;
    private Label lbDataNascimento;
    private Label lbSexo;
    //*********************************
    protected TextField txtId;
    private TextField txtNome;
    private TextField txtCPF;
    private TextField txtRG;
    private TextField txtPassaporte;
    private DatePicker dtDataNascimento;
    private ComboBox<Sexo> cbSexo;
    /* Cadastro de Endereço */
    private Label lbNumero;
    private Label lbCEP;
    private Label lbRua;
    private Label lbBairro;
    private Label lbComplemento;
    private Label lbPais;
    private Label lbUF;
    private Label lbCidade;
    //*********************************
    private TextField txtNumero;
    private TextField txtCEP;
    private TextField txtRua;
    private TextField txtBairro;
    private TextField txtComplemento;
    private TextField txtPais;
    private ComboBox<Estado> cbUF;
    private ComboBox<Cidade> cbCidade;
    //*********************************
    protected Button btnSalvarAtualizar;
    protected Button btnDeletar;
    protected Button btnLimparTela;
    private Button btnBuscaPais;
    //*********************************
    private HBox hbBotoesCRUD;
    //*********************************
    private GridPane gpCadastroPessoaLayout;
    private GridPane gpCadastroEnderecoLayout;
    private BorderPane bpLayoutGeral;
    private Accordion adnDadosExtras; //Engloba endereço, entro outros TitlePanes que venham a ser adicionados.
    private TitledPane tpEndereco;
    //*********************************
    private ObservableList<Sexo> sexos;
    private ObservableList<Estado> ufs;
    private ObservableList<Cidade> cidades;

    protected void iniciaCombosBoxes()
    {
        instanciaController();
        this.sexos = FXCollections.observableArrayList(Sexo.values());
        this.ufs = FXCollections.observableArrayList(GeralUtilDAO.listaEstados());
        this.cbSexo.setItems(sexos);
        this.cbUF.setItems(ufs);
        this.cbUF.getSelectionModel().select(1);
        this.cidades = FXCollections.observableArrayList(GeralUtilDAO
                .listaCidadesDeAcordoComOEstadoSelecionado("AC"));
        this.cbCidade.setItems(cidades);
    }

    protected final void iniciaFieldsDeCadastroPessoa()
    {
        this.txtId = new TextField();
        this.txtNome = new TextField();
        this.txtCPF = new CPFECNPJTextField();
        this.txtRG = new TextField();
        this.txtPassaporte = new TextField();
        //**************************************
        this.dtDataNascimento = new DatePicker(LocalDate.now());
        this.cbSexo = new ComboBox();
        //**************************************
        FormattedTextFieldUtil.setNumericValidation(txtId, 10);
        FormattedTextFieldUtil.setLetterValidation(txtNome, 50);
        FormattedTextFieldUtil.addTextLimiter(txtPassaporte, 40);
    }

    protected final void iniciaLabelsDeCadastroPessoa()
    {
        lbId = new Label("Código.:");
        lbNome = new Label("Nome.:");
        lbCFP = new Label("CPF.:");
        lbRG = new Label("RG.:");
        lbPassaporte = new Label("Passaporte.:");
        lbDataNascimento = new Label("Data Nascimento.:");
        lbSexo = new Label("Sexo.:");
    }

    protected final void iniciaLayoutDeCadastroPessoa()
    {
        this.gpCadastroPessoaLayout = new GridPane();
        LayoutManagerUtil.personalizaGriPane(gpCadastroPessoaLayout);
        gpCadastroPessoaLayout.setAlignment(Pos.CENTER);
        //Node, Coluna, Linha
        GridPane.setConstraints(lbId, 0, 0);
        GridPane.setConstraints(txtId, 1, 0);
        //*************************************
        GridPane.setConstraints(lbNome, 2, 0);
        GridPane.setConstraints(txtNome, 3, 0);
        //*************************************
        GridPane.setConstraints(lbCFP, 0, 1);
        GridPane.setConstraints(txtCPF, 1, 1);
        //*************************************
        GridPane.setConstraints(lbRG, 2, 1);
        GridPane.setConstraints(txtRG, 3, 1);
        //*************************************
        GridPane.setConstraints(lbPassaporte, 0, 2);
        GridPane.setConstraints(txtPassaporte, 1, 2);
        //*************************************
        GridPane.setConstraints(lbDataNascimento, 2, 2);
        GridPane.setConstraints(dtDataNascimento, 3, 2);
        //*************************************
        GridPane.setConstraints(lbSexo, 0, 4);
        GridPane.setConstraints(cbSexo, 1, 4);
        //*************************************
        GridPane.setConstraints(hbBotoesCRUD, 0, 5);
        //*************************************
        gpCadastroPessoaLayout.getChildren().addAll(lbId,
                                                    lbNome,
                                                    lbCFP,
                                                    lbRG,
                                                    lbPassaporte,
                                                    lbDataNascimento,
                                                    lbSexo,
                                                    txtId,
                                                    txtNome,
                                                    txtCPF,
                                                    txtRG,
                                                    txtPassaporte,
                                                    dtDataNascimento,
                                                    cbSexo);
    }

    protected final void iniciaLabelsDeCadastroEndereco()
    {
        this.lbNumero = new Label("Número.:");
        this.lbCEP = new Label("CEP.:");
        this.lbRua = new Label("Rua.:");
        this.lbBairro = new Label("Bairro.:");
        this.lbComplemento = new Label("Complemento.:");
        this.lbPais = new Label("Pais.:");
        this.lbUF = new Label("UF.:");
        this.lbCidade = new Label("Cidade.:");
    }

    protected final void iniciaFieldDeCadastroEndereco()
    {
        this.txtNumero = new TextField();
        this.txtCEP = new TextField();
        this.txtRua = new TextField();
        this.txtBairro = new TextField();
        this.txtComplemento = new TextField();
        this.txtPais = new TextField();
        this.txtPais.setEditable(false);
        //**************************************
        this.cbUF = new ComboBox();
        this.cbCidade = new ComboBox();
        //**************************************
        FormattedTextFieldUtil.setNumericValidation(txtNumero, 10);
    }

    protected final void iniciaBotaoDeEndereco()
    {
        Image imagem = new Image(getClass().getResourceAsStream("/com/imgs/icone_busca.png"));
        ImageView iconeBusca = new ImageView(imagem);
        this.btnBuscaPais = new Button("", iconeBusca);
    }

    protected final void iniciaLayoutDeCadastroEndereco()
    {
        this.gpCadastroEnderecoLayout = new GridPane();
        LayoutManagerUtil.personalizaGriPane(gpCadastroEnderecoLayout);
        gpCadastroEnderecoLayout.setAlignment(Pos.CENTER);
        //Node, Coluna, Linha
        GridPane.setConstraints(lbNumero, 0, 0);
        GridPane.setConstraints(txtNumero, 1, 0);
        //**************************************
        GridPane.setConstraints(lbCEP, 2, 0);
        GridPane.setConstraints(txtCEP, 3, 0);
        //**************************************
        GridPane.setConstraints(lbRua, 0, 1);
        GridPane.setConstraints(txtRua, 1, 1);
        //**************************************
        GridPane.setConstraints(lbBairro, 2, 1);
        GridPane.setConstraints(txtBairro, 3, 1);
        //**************************************
        GridPane.setConstraints(lbComplemento, 0, 2);
        GridPane.setConstraints(txtComplemento, 1, 2);
        //**************************************
        GridPane.setConstraints(lbPais, 2, 2);
        GridPane.setConstraints(txtPais, 3, 2);
        GridPane.setConstraints(btnBuscaPais, 4, 2);
        //**************************************
        GridPane.setConstraints(lbUF, 0, 3);
        GridPane.setConstraints(cbUF, 1, 3);
        //**************************************
        GridPane.setConstraints(lbCidade, 2, 3);
        GridPane.setConstraints(cbCidade, 3, 3);
        //**************************************
        gpCadastroEnderecoLayout.getChildren().addAll(lbNumero,
                                                      lbCEP,
                                                      lbRua,
                                                      lbBairro,
                                                      lbComplemento,
                                                      lbPais,
                                                      lbUF,
                                                      lbCidade,
                                                      txtNumero,
                                                      txtCEP,
                                                      txtRua,
                                                      txtBairro,
                                                      txtComplemento,
                                                      txtPais,
                                                      btnBuscaPais,
                                                      cbUF,
                                                      cbCidade);
        this.tpEndereco = new TitledPane("Endereço a ser Cadastrado \\ Atualizado", gpCadastroEnderecoLayout);
    }

    protected final void iniciaAccordionDeDadosExtras()
    {
        this.adnDadosExtras = new Accordion(tpEndereco);
        this.adnDadosExtras.setPadding(new Insets(10));
    }

    protected final void iniciaLayoutGeral()
    {
        this.bpLayoutGeral = new BorderPane();
        bpLayoutGeral.setPadding(new Insets(10));
        bpLayoutGeral.setTop(gpCadastroPessoaLayout);
        bpLayoutGeral.setBottom(hbBotoesCRUD);
        bpLayoutGeral.setCenter(adnDadosExtras);
    }

    protected final void iniciaBotoesDeUtilizacaoGeral()
    {
        btnSalvarAtualizar = new Button("Salvar");
        btnDeletar = new Button("Deletar");
        btnLimparTela = new Button("Limpar Tela");
        hbBotoesCRUD = new HBox();
        hbBotoesCRUD.setSpacing(10);
        hbBotoesCRUD.setAlignment(Pos.CENTER);
        hbBotoesCRUD.getChildren().addAll(btnSalvarAtualizar,
                                          btnDeletar,
                                          btnLimparTela);
    }

    /**
     * MC = MouseClicked
     */
    protected void MCAjustrTelaAoMinOuMaxNoTitlePane()
    {
        LayoutManagerUtil.SetMCEfeitoDeReduzirExpaendirTelaTitledPane(tpEndereco, getWindow());
    }

    /**
     * AE = ActionEvent.
     */
    protected final void AELimparTela()
    {
        btnLimparTela.setOnAction((e) ->
        {
            limparCampos();
            txtId.requestFocus();
            setUltimoNumero();
        });
    }

    /* INICIO DOS MÉTODOS CRUD*/
    /**
     * AE = ActionEvent.
     */
    protected void AESalvaOuAtualiza()
    {
        btnSalvarAtualizar.setOnAction((e) ->
        {
            try
            {
                setDadosPessoaEnderecoNoObj();
                if (pessoa != null)
                {
                    controller.salvaOuAtualiza(pessoa);
                    if (controller.isValidadoComSucesso())
                    {
                        limparCampos();
                        resetaTela();
                    }
                }
            }
            catch (Exception ex)
            {
                new AlertaExceptionUtil(ex).mostra();
            }
        });
    }

    /**
     * AE = ActionEvent.
     */
    protected void AEDeleta()
    {
        btnDeletar.setOnAction((e) ->
        {
            try
            {
                if (pessoa != null && pessoa.getId() != null)
                {
                    pessoa.setId(Long.parseLong(txtId.getText()));
                    if (pessoa.getId().longValue() > 0)
                    {
                        controller.deleta(pessoa);
                        limparCampos();
                        resetaTela();
                    }
                }
            }
            catch (Exception ex)
            {
                new AlertaExceptionUtil(ex).mostra();
            }
        });
    }

    /**
     * CL = ChangeListener. Executa a busca da entidade, quando o foco é
     * retirado do txtId ou coloca o ultimo código da pessoa + 1.
     */
    protected void CLBusca()
    {
        txtId.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) ->
        {
            if (!newValue)
            {
                try
                {
                    Long id = 0L;
                    if (isNumber(txtId.getText()))
                    {
                        id = Long.parseUnsignedLong(txtId.getText());
                    }

                    if (id > 0L)
                    {
                        this.pessoa = retornaNovaInstanciaDeClasseFilha(id);
                        pessoa = (T) controller.busca(pessoa);
                        if (pessoa != null)
                        {
                            colocaReferenciaDeObjHerdeiraEmPessoa(pessoa);
                            exibeDadosPessoaEndereco();
                        }
                        else
                        {
                            limparCampos();
                        }
                    }
                }
                catch (Exception e)
                {
                    new AlertaExceptionUtil(e).mostra();
                }
            }
        });
    }

    /* FIM DOS MÉTODOS CRUD*/
    /**
     * AE = ActionEvent.
     */
    protected final void AEMudaCidadesDoComboAoMudarUF()
    {
        try
        {
            this.cbCidade.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) ->
            {
                if (newValue)
                {
                    String uf = cbUF.getSelectionModel().selectedItemProperty().getValue().toString().trim();
                    if (uf != null && !uf.equals(""))
                    {
                        List<Cidade> cidadesSelecionadas = GeralUtilDAO.listaCidadesDeAcordoComOEstadoSelecionado(uf);
                        cidades.clear();
                        cidades.addAll(cidadesSelecionadas);
                    }
                }
            });
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    /**
     * AE = ActionEvent.
     */
    protected final void AELimpaCidadesDoComboAoMudarUF()
    {
        try
        {
            this.cbUF.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) ->
            {
                if (newValue)
                {
                    cidades.clear();
                }
            });
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    /**
     * AE = ActionEvent.
     */
    protected final void AEBuscaPais()
    {
        try
        {
            Pane content = this.getWindow().getContentPane();
            this.btnBuscaPais.setOnAction((e) ->
            {
                content.getChildren().remove(importador.getWindow());
                this.importador = new ImportaPaisesView();
                content.getChildren().add(importador.getWindow());
                configuraImportador();
            });
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    protected void exibeDadosPessoaEndereco()
    {
        try
        {
            if (this.pessoa != null)
            {
                /* Dados Pessoa (INICIO) */
                LocalDate tempLocalDate = TempoUtil.converteCalendarParaLocalDate(pessoa.getDataNascimento());
                this.txtId.setText(String.valueOf(pessoa.getId()));
                this.txtNome.setText(pessoa.getNome());
                this.txtCPF.setText(pessoa.getCpf());
                this.txtRG.setText(pessoa.getRg());
                this.txtPassaporte.setText(pessoa.getPassaporte());
                this.dtDataNascimento.setValue(tempLocalDate);
                this.cbSexo.getSelectionModel().select(pessoa.getSexo());
                /* Dados Pessoa (FIM) */
                //*******************************************
                /* Dados Endereço (INICIO) */
                this.endereco = (E) pessoa.getEndereco();
                if (this.endereco != null)
                {
                    Cidade cidade = endereco.getCidade();
                    this.paisImportado = cidade.getEstado().getPais();
                    if (this.paisImportado != null)
                    {
                        this.txtPais.setText(paisImportado.getNome());
                    }
                    else
                    {
                        this.txtPais.setText("EXTERIOR");
                    }
                    this.txtNumero.setText(String.valueOf(endereco.getNumero()));
                    this.txtCEP.setText(endereco.getCep());
                    this.txtRua.setText(endereco.getRua());
                    this.txtBairro.setText(endereco.getBairro());
                    this.txtComplemento.setText(endereco.getComplemento());
                    this.cbUF.setValue(cidade.getEstado());
                    this.cbCidade.setValue(cidade);
                    /* Dados Endereço (FIM) */
                }
            }
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    protected void setDadosPessoaEnderecoNoObj()
    {
        try
        {
            /* Pessoa (INICIO) */
            colocaReferenciaDeObjHerdeiraEmPessoa();
            Long id = 0L;
            if (isNumber(txtId.getText()))
            {
                id = Long.parseUnsignedLong(txtId.getText());
            }
            String nome = txtNome.getText();
            String cpf = txtCPF.getText();
            String rg = txtRG.getText();
            String passaporte = txtPassaporte.getText();
            Calendar dataNascimento = Calendar.getInstance();
            if (dtDataNascimento.getValue() != null)
            {
                Instant instant = dtDataNascimento.getValue()
                        .atStartOfDay()
                        .atZone(ZoneId.systemDefault()).toInstant();
                Date dateTemp = Date.from(instant);
                dataNascimento.setTime(dateTemp);
            }
            Sexo sexo = cbSexo.getSelectionModel().getSelectedItem();
            pessoa.setId(id);
            pessoa.setNome(nome);
            pessoa.setCpf(cpf);
            pessoa.setRg(rg);
            pessoa.setPassaporte(passaporte);
            pessoa.setDataNascimento(dataNascimento);
            pessoa.setSexo(sexo);
            /* Pessoa (FIM) */
            //*************************************
            /* Endereço (INICIO) */
            colocaReferenciaDeObjHerdeiraEmEndereco();
            Integer numero = 0;
            if (isNumber(txtNumero.getText()))
            {
                numero = Integer.parseUnsignedInt(txtNumero.getText());
            }
            String cep = txtCEP.getText();
            String rua = txtRua.getText();
            String bairro = txtBairro.getText();
            String complemento = txtComplemento.getText();
            Cidade cidade = cbCidade.getSelectionModel().getSelectedItem();
            Estado uf = cbUF.getSelectionModel().getSelectedItem();
            if (uf != null)
            {
                uf.setPais(paisImportado);
            }
            if (cidade != null && uf != null)
            {
                cidade.setEstado(uf);
            }
            endereco.setId(pessoa.getId());
            endereco.setNumero(numero);
            endereco.setCep(cep);
            endereco.setRua(rua);
            endereco.setBairro(bairro);
            endereco.setComplemento(complemento);
            endereco.setCidade(cidade);
            endereco.setPessoa(pessoa);
            /* Endereço (FIM) */
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    /**
     * "Limpa" os TextFields e reseta os comboboxes.
     */
    protected void limparCampos()
    {
        try
        {
            for (Node no : gpCadastroPessoaLayout.getChildren())
            {
                if (no instanceof TextField)
                {
                    TextField aSerLimpado = (TextField) no;
                    aSerLimpado.clear();
                }
            }

            for (TitledPane tp : adnDadosExtras.getPanes())
            {
                GridPane gpConteudo = (GridPane) tp.getContent();
                for (Node no : gpConteudo.getChildren())
                {
                    if (no instanceof TextField)
                    {
                        TextField aSerLimpado = (TextField) no;
                        aSerLimpado.clear();
                    }
                }
            }

            dtDataNascimento.setValue(LocalDate.now());
            cbSexo.getSelectionModel().selectFirst();
            cbUF.getSelectionModel().selectFirst();
            setUltimoNumero();

        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    @Override
    protected final void setUltimoNumero()
    {
        try
        {
            Long ultimo = getUltimoNumeroDeEntidade();
            txtId.setText(String.valueOf(ultimo));
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    /* Métodos relacionados com o importador de países (FIM) */
    protected final boolean isNumber(String number)
    {
        return number.trim().matches("[0-9]+");
    }

    protected final void addTitlePaneEmAccordionDeDadosExtras(TitledPane tpNovoConteudo)
    {
        if (tpNovoConteudo != null)
        {
            adnDadosExtras.getPanes().add(tpNovoConteudo);
        }
        else
        {
            throw new NullPointerException("O TitledPane inserido não pode ser nulo.");
        }
    }

    protected BorderPane getBpLayoutGeral()
    {
        return bpLayoutGeral;
    }

    protected GridPane getGpCadastroPessoaLayout()
    {
        return gpCadastroPessoaLayout;
    }

    protected Accordion getAdnDadosExtras()
    {
        return adnDadosExtras;
    }

    protected HBox getHbBotoesCRUD()
    {
        return hbBotoesCRUD;
    }

    @Override
    public void permiteEdicao()
    {
        try
        {
            for (Node no : getGpCadastroPessoaLayout().getChildren())
            {
                habilitaCamposBotoes(no);
            }

            for (TitledPane tp : getAdnDadosExtras().getPanes())
            {
                GridPane gpConteudo = (GridPane) tp.getContent();
                for (Node no : gpConteudo.getChildren())
                {
                    habilitaCamposBotoes(no);
                }
            }

            for (Node no : getHbBotoesCRUD().getChildren())
            {
                habilitaCamposBotoes(no);
            }

            this.txtPais.setEditable(false);
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    protected TitledPane getTpEndereco()
    {
        return tpEndereco;
    }

    @Override
    public void permiteApenasVisualizacao()
    {
        try
        {
            for (Node no : getGpCadastroPessoaLayout().getChildren())
            {
                desabilitaCamposBotoes(no);
            }

            for (TitledPane tp : getAdnDadosExtras().getPanes())
            {
                GridPane gpConteudo = (GridPane) tp.getContent();
                for (Node no : gpConteudo.getChildren())
                {
                    desabilitaCamposBotoes(no);
                }
            }

            for (Node no : getHbBotoesCRUD().getChildren())
            {
                desabilitaCamposBotoes(no);
            }
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    /* Métodos relacionados com o importador de países (COMEÇO) */
    @Override
    public void configuraImportador()
    {
        try
        {
            this.importador.setClienteDoImportador(this);
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    public Window getImportadorWindow()
    {
        try
        {
            return importador.getWindow();
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }

        return null;
    }

    @Override
    public void setItem(Pais pais)
    {
        try
        {
            this.paisImportado = pais;
            this.txtPais.setText(pais.getNome());
            //1058 é o código do país Brasil
            if (!paisImportado.getCodigo().trim().equals("1058"))
            {
                //Selecione o estado como EX (exterior)
                this.cbUF.getSelectionModel().selectLast();
            }
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    /* Métodos relacionados com o importador de países (FIM) */
    /**
     * Método utilizado pela TabelaView para passar a entidade para a View.
     *
     * @param entidade A entidade selecionada na TabelaView.
     */
    @Override
    public final void setEntidade(T entidade)
    {
        Objects.requireNonNull(entidade, "A entidade passada não pode ser nula.");
        //É feito a busca novamente para que o EntityManager gerencie essa entidade novamente.
        this.pessoa = (T) controller.busca(entidade);
        exibeDadosPessoaEndereco();
    }

    /**
     * Utilizado para colocar a referência da classe filha de pessoa na classe
     * pai pessoa.
     */
    protected abstract void colocaReferenciaDeObjHerdeiraEmPessoa();

    /**
     * Utilizado para colocar a referência da classe filha de pessoa na classe
     * pai pessoa. Sobrecarga.
     *
     * @param pessoa
     */
    protected abstract void colocaReferenciaDeObjHerdeiraEmPessoa(T pessoa);

    /**
     * A classe filha deve retornar uma nova instância passando o id da mesma no
     * construtor.
     *
     * @param ID
     *
     * @return Nova instância da classe filha
     */
    protected abstract T retornaNovaInstanciaDeClasseFilha(Number ID);

    /**
     * Utilizado para colocar a referência da classe filha de endereço na classe
     * pai endereço.
     */
    protected abstract void colocaReferenciaDeObjHerdeiraEmEndereco();

    /**
     * O controller deve ser instânciado para que seja possível fazer a busca e
     * exibir a entidade sem que ocorra um nullpointer exception.
     */
    protected abstract void instanciaController();

}
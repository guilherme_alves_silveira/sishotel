package com.gui.util;

import java.util.Objects;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import jfxtras.labs.scene.control.window.Window;

public class LayoutManagerUtil
{
    public synchronized static final void personalizaGriPane(final GridPane paneASerPersonalizada)
    {
        paneASerPersonalizada.setPadding(new Insets(5));
        paneASerPersonalizada.setVgap(5);
        paneASerPersonalizada.setHgap(5);
    }

    public synchronized static void SetMCEfeitoDeReduzirExpaendirTelaTitledPane(final TitledPane pane, final Window window)
    {
        Objects.requireNonNull(pane, "O TitlePane não poder ser nulo.");

        pane.setOnMouseClicked((MouseEvent e) ->
        {
            try
            {
                if (!pane.isExpanded())
                {
                    reduzTelaSuavemente(window);
                }
                else
                {
                    expandeTelaSuavemente(window);
                }
            }
            catch (Throwable ex)
            {
                new AlertaExceptionUtil(ex).mostra();
            }
        });
    }
    
    public static void reduzTelaSuavemente(final Window window)
    {
        try
        {
            Task<Void> taskReduzTelaSuavemente = new Task<Void>()
            {
                @Override
                protected Void call() throws Exception
                {
                    for (double tamanho = window.getHeight(); tamanho > window.getMinHeight(); tamanho -= 10)
                    {
                        try
                        {
                            Thread.sleep(10);
                        }
                        catch (InterruptedException ex)
                        { /* Ignora */ }
                        window.setPrefSize(window.getWidth(), tamanho);
                    }

                    return null;
                }
            };
            Thread segmento = new Thread(taskReduzTelaSuavemente);
            segmento.start();
        }
        catch (Throwable e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    public static void expandeTelaSuavemente(final Window window)
    {
        try
        {
            Task<Void> taskExpandeTelaSuavemente = new Task<Void>()
            {
                @Override
                protected Void call() throws Exception
                {
                    for (double tamanho = window.getHeight(); tamanho < window.getMaxHeight() + 10; tamanho += 10)
                    {
                        try
                        {
                            Thread.sleep(10);
                        }
                        catch (InterruptedException ex)
                        { /* Ignora */ }
                        window.setPrefSize(window.getWidth(), tamanho);
                    }

                    return null;
                }
            };
            Thread segmento = new Thread(taskExpandeTelaSuavemente);
            segmento.start();
        }
        catch (Throwable e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }
}
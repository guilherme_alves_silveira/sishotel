package com.gui.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class AlertaExceptionUtil
{
    private final Alert alert = new Alert(AlertType.ERROR);
    private static final StringWriter sw = new StringWriter();
    private static final PrintWriter pw = new PrintWriter(sw);
    private static final TextArea txtaExcecao = new TextArea();
    private static final GridPane gpConteudo = new GridPane();
    private static final Label label = new Label("O erro foi:");

    static
    {
        gpConteudo.setMaxWidth(Double.MAX_VALUE);
        gpConteudo.add(label, 0, 0);
        gpConteudo.add(txtaExcecao, 0, 1);
    }

    public AlertaExceptionUtil(Throwable ex)
    {
        alert.setTitle("Alerta de Erro");
        alert.setHeaderText("ERRO");
        alert.setContentText("Contate o administrator do sistema!");
        ex.printStackTrace(pw);
        String excecaoTexto = sw.toString();
        txtaExcecao.setText("");
        txtaExcecao.setText(excecaoTexto);
        txtaExcecao.setEditable(false);
        txtaExcecao.setWrapText(true);
        txtaExcecao.setMaxWidth(Double.MAX_VALUE);
        txtaExcecao.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(txtaExcecao, Priority.ALWAYS);
        GridPane.setHgrow(txtaExcecao, Priority.ALWAYS);
        alert.getDialogPane().setExpandableContent(gpConteudo);
    }

    public void mostra()
    {
        alert.showAndWait();
    }
}
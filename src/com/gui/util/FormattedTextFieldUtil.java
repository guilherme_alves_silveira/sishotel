package com.gui.util;

import java.util.HashSet;
import java.util.Set;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class FormattedTextFieldUtil
{
    public static void setNumericValidation(TextField tf, final Integer maxLenght)
    {
        tf.setOnKeyTyped(numericValidation(maxLenght));
    }
    
    public static void setLetterValidation(TextField tf, final Integer maxLenght)
    {
        tf.setOnKeyTyped(letterValidation(maxLenght));
    }

    private static EventHandler<KeyEvent> numericValidation(final Integer maxLenght)
    {
        return (e) ->
        {
            TextField tf = (TextField) e.getSource();
            if (tf.getText().length() >= maxLenght)
            {
                e.consume();
            }
            if (e.getCharacter().matches("[0-9.]"))
            {
                if (tf.getText().contains(".") && e.getCharacter().matches("[.]"))
                {
                    e.consume();
                }
                else if (tf.getText().length() == 0 && e.getCharacter().matches("[.]"))
                {
                    e.consume();
                }
            }
            else
            {
                e.consume();
            }
        };
    }

    private static EventHandler<KeyEvent> letterValidation(final Integer maxLenght)
    {
        return (e) ->
        {
            TextField tf = (TextField) e.getSource();
            if (tf.getText().length() >= maxLenght)
            {
                e.consume();
            }
            if (e.getCharacter().matches("[A-Za-z,.'\"ãâóíìéè\\s]"))
            {
                
            }
            else
            {
                e.consume();
            }
        };
    }
    
    public static void addMask(final TextField tf, final String mask)
    {
        tf.setText(mask);
        addTextLimiter(tf, mask.length());

        tf.textProperty().addListener((final ObservableValue<? extends String> ov, final String oldValue, final String newValue) ->
        {
            String value = stripMask(tf.getText(), mask);
            tf.setText(merge(value, mask));
        });

        tf.setOnKeyPressed((final KeyEvent e) ->
        {
            int caretPosition = tf.getCaretPosition();
            if (caretPosition < mask.length() - 1 && mask.charAt(caretPosition) != ' ' && e.getCode() != KeyCode.BACK_SPACE && e.getCode() != KeyCode.LEFT)
            {
                tf.positionCaret(caretPosition + 1);
            }
        });
    }

    static String merge(final String value, final String mask)
    {
        final StringBuilder sb = new StringBuilder(mask);
        int k = 0;
        for (int i = 0; i < mask.length(); i++)
        {
            if (mask.charAt(i) == ' ' && k < value.length())
            {
                sb.setCharAt(i, value.charAt(k));
                k++;
            }
        }
        return sb.toString();
    }

    static String stripMask(String text, final String mask)
    {
        final Set<String> maskChars = new HashSet<>();
        for (int i = 0; i < mask.length(); i++)
        {
            char c = mask.charAt(i);
            if (c != ' ')
            {
                maskChars.add(String.valueOf(c));
            }
        }
        for (String c : maskChars)
        {
            text = text.replace(c, "");
        }
        return text;
    }

    public static void addTextLimiter(final TextField tf, final int maxLength)
    {
        tf.textProperty().addListener((final ObservableValue<? extends String> ov, final String oldValue, final String newValue) ->
        {
            if (tf.getText().length() > maxLength)
            {
                String s = tf.getText().substring(0, maxLength);
                tf.setText(s);
            }
        });
    }
}

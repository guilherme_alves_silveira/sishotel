package com.model.dao;

import com.model.po.Quarto;
import java.util.List;
import javax.persistence.EntityManager;

public class QuartoDAO extends GenericoDAO<Quarto, Short>
{
    public QuartoDAO(EntityManager em)
    {
        super(em, Quarto.class);
    }

    @Override
    public List<Quarto> listaTodos()
    {
        return getEntityManager().createNamedQuery("Quarto.findAll")
                                 .getResultList();
    }
}
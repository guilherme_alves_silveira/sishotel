package com.model.dao;

import com.model.po.Categoria;
import com.model.po.Cidade;
import com.model.po.Estado;
import com.model.po.Funcao;
import com.model.po.Pais;
import com.util.JPAUtil;
import java.util.List;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class GeralUtilDAO
{

    private static EntityManager em;
    private static final EnderecoUtilDAOFiltro filtroDAO;
    private static List<Funcao> funcaofindAllConstructor;
    private static List<Estado> estadofindAllConstructor;
    private static Long quantidadeDePaises;

    static
    {
        em = JPAUtil.getEntityManager();
        filtroDAO = new EnderecoUtilDAOFiltro();
    }

    private GeralUtilDAO()
    {
        //Construtor privado
    }

    public static List<Estado> listaEstados()
    {
        inicia();
        try
        {

            if (estadofindAllConstructor == null || estadofindAllConstructor.isEmpty())
            {
                TypedQuery<Estado> readQuery = em.createNamedQuery("Estado.findAllConstructor", Estado.class);
                readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
                estadofindAllConstructor = readQuery.getResultList();
            }
            return estadofindAllConstructor;
        }
        finally
        {
            fechaConexao();
        }
    }

    public static List<Funcao> listaFuncoesDeFuncionario()
    {
        inicia();
        try
        {
            if (funcaofindAllConstructor == null || funcaofindAllConstructor.isEmpty())
            {
                TypedQuery<Funcao> readQuery = em.createNamedQuery("Funcao.findAll", Funcao.class);
                readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
                funcaofindAllConstructor = readQuery.getResultList();
            }
            return funcaofindAllConstructor;
        }
        finally
        {
            fechaConexao();
        }
    }

    public static List<Categoria> listaCategorias()
    {
        inicia();
        try
        {
            TypedQuery<Categoria> readQuery = em.createNamedQuery("Categoria.findAll", Categoria.class);
            readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
            return readQuery.getResultList();
        }
        finally
        {
            fechaConexao();
        }
    }

    public static List<Cidade> listaCidadesDeAcordoComOEstadoSelecionado(String uf)
    {
        inicia();
        try
        {
            TypedQuery<Cidade> readQuery = em.createNamedQuery("Cidade.findAllConstructorByUF", Cidade.class);
            readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
            readQuery.setParameter("uf", uf);
            return readQuery.getResultList();
        }
        finally
        {
            fechaConexao();
        }
    }

    public static List<Pais> buscaPaginadaDePaises(int primeiroResultado, int quantidadeDeResultados)
    {
        inicia();
        try
        {
            TypedQuery<Pais> readQuery = em.createNamedQuery("Pais.findAllPaises", Pais.class);
            readQuery.setFirstResult(primeiroResultado)
                    .setMaxResults(quantidadeDeResultados);
            return readQuery.getResultList();
        }
        finally
        {
            fechaConexao();
        }
    }

    public static Long buscaTotalDePais()
    {
        inicia();
        try
        {
            if (quantidadeDePaises == null || quantidadeDePaises <= 0)
            {
                TypedQuery<Long> readQuery = em.createQuery("SELECT COUNT(p) FROM Pais p", Long.class);
                quantidadeDePaises = readQuery.getSingleResult();
            }
            return quantidadeDePaises;
        }
        finally
        {
            fechaConexao();
        }
    }

    public static List<Pais> buscaListaFiltradaDePaises(Pais entidade, String... propriedades)
    {
        return filtroDAO.buscaListaFiltrada(entidade, propriedades);
    }

    private static class EnderecoUtilDAOFiltro extends GenericoDAO<Pais, Short>
    {

        public EnderecoUtilDAOFiltro()
        {
            super(em, Pais.class);
        }

        @Override
        public List listaTodos()
        {
            return em.createNamedQuery("Pais.findAllPaises", Pais.class)
                    .getResultList();
        }

        public List<Pais> buscaListaFiltrada(Pais entidade, String... propriedades)
        {
            inicia();
            try
            {
                return super.filtrar(entidade, propriedades);
            }
            finally
            {
                fechaConexao();
            }
        }
    }

    public static void inicia()
    {
        if (em == null || !em.isOpen())
        {
            em = JPAUtil.getEntityManager();
        }
    }

    public static void fechaConexao()
    {
        JPAUtil.closeEntityManager();
    }
}

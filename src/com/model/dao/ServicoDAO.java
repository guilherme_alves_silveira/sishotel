package com.model.dao;

import com.model.po.Servico;
import java.util.List;
import javax.persistence.EntityManager;

public class ServicoDAO extends BorderoDAO<Servico>
{
    public ServicoDAO(EntityManager em)
    {
        super(em, Servico.class);
    }

    @Override
    public List<Servico> listaTodos()
    {
        return getEntityManager().createNamedQuery("Servico.findAll")
                                 .getResultList();
    }
}
package com.model.dao;

import com.model.po.Dependente;
import java.util.List;
import javax.persistence.EntityManager;

public class DependenteDAO extends GenericoDAO<Dependente, Long>
{
    public DependenteDAO(EntityManager em)
    {
        super(em, Dependente.class);
    }

    @Override
    public List<Dependente> listaTodos()
    {
        return getEntityManager().createNamedQuery("Dependente.findAll")
                                 .getResultList();
    }
}

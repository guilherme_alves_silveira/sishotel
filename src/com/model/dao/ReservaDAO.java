package com.model.dao;

import com.model.po.Reserva;
import java.util.List;
import javax.persistence.EntityManager;

public class ReservaDAO extends GenericoDAO<Reserva, Long>
{
    public ReservaDAO(EntityManager em)
    {
        super(em, Reserva.class);
    }

    @Override
    public List<Reserva> listaTodos()
    {
        return getEntityManager().createNamedQuery("Reserva.findAll'")
                                 .getResultList();
    }
}
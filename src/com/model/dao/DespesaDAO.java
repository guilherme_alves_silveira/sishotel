package com.model.dao;

import com.model.po.Despesa;
import java.util.List;
import javax.persistence.EntityManager;

public class DespesaDAO extends BorderoDAO<Despesa>
{
    public DespesaDAO(EntityManager em)
    {
        super(em, Despesa.class);
    }

    @Override
    public List<Despesa> listaTodos()
    {
        return getEntityManager().createNamedQuery("Despesa.findAll")
                                 .getResultList();
    }
}
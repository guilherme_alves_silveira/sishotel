package com.model.dao;

import com.model.po.Categoria;
import java.util.List;
import javax.persistence.EntityManager;

public class CategoriaDAO extends GenericoDAO<Categoria, Short>
{
    public CategoriaDAO(EntityManager em)
    {
        super(em, Categoria.class);
    }
    
    @Override
    public List<Categoria> listaTodos()
    {
        return getEntityManager().createNamedQuery("Categoria.findAll")
                                 .getResultList();
    }
}
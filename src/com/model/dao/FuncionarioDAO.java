package com.model.dao;

import com.model.po.Funcionario;
import java.util.List;
import javax.persistence.EntityManager;

public class FuncionarioDAO extends GenericoDAO<Funcionario, Long>
{
    public FuncionarioDAO(EntityManager em)
    {
        super(em, Funcionario.class);
    }

    @Override
    public List<Funcionario> listaTodos()
    {
        return getEntityManager().createNamedQuery("Funcionario.findAll")
                                 .getResultList();
    }
}
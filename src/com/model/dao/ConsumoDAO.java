package com.model.dao;

import com.model.po.ConsumoCabecalho;
import java.util.List;
import javax.persistence.EntityManager;

public class ConsumoDAO extends GenericoDAO<ConsumoCabecalho, Long>
{

    public ConsumoDAO(EntityManager em)
    {
        super(em, ConsumoCabecalho.class);
    }

    @Override
    public List<ConsumoCabecalho> listaTodos()
    {
        return getEntityManager().createNamedQuery("ConsumoCabecalho.findAll")
                                 .getResultList();
    }
}
package com.model.dao;

import com.model.po.Hospede;
import java.util.List;
import javax.persistence.EntityManager;

public class HospedeDAO extends GenericoDAO<Hospede, Long>
{
    public HospedeDAO(EntityManager em)
    {
        super(em, Hospede.class);
    }

    @Override
    public List<Hospede> listaTodos()
    {
        return getEntityManager().createNamedQuery("Hospede.findAll")
                                 .getResultList();
    }
}
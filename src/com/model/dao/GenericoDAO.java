package com.model.dao;

import com.model.base.Entidade;
import com.util.JPAUtil;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.beanutils.PropertyUtils;

public abstract class GenericoDAO<T extends Entidade, ID extends Number>
{
    private EntityManager em;
    private Class<T> classeEntidade;

    public GenericoDAO(EntityManager em, Class<T> classeEntidade)
    {
        if (em != null && classeEntidade != null)
        {
            this.em = em;
            this.classeEntidade = classeEntidade;
        }
        else
        {
            throw new NullPointerException("O EntityManager não pode estar nulo.");
        }
    }

    public void salvaOuAtualiza(T entidade)
    {
        if (isNotNull(entidade))
        {
            em.merge(entidade);
        }
        else
        {
            throw new NullPointerException("A entidade não pode estar nulo.");
        }
    }

    public void deleta(T entidade)
    {
        if (isNotNull(entidade) && (entidade.getId() != null))
        {
            em.remove(entidade);
        }
        else
        {
            throw new NullPointerException("A entidade não pode estar nulo.");
        }
    }

    public T pegaPorId(ID codigo)
    {
        T entidade = null;
        try
        {
            entidade = em.find(classeEntidade, String.valueOf(codigo));
        }
        catch (EntityNotFoundException e)
        {
            //Não faz nada, pois a entidade não foi encontrada
        }
        return entidade;
    }

    @SuppressWarnings("unchecked")
    public List<T> filtrar(T entidade, String... propriedades)
    {
        inicia();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteria = (CriteriaQuery<T>) builder.createQuery(classeEntidade);
        Root<T> root = (Root<T>) criteria.from(classeEntidade);
        List<Predicate> predicates = new ArrayList<>();
        if (propriedades != null && propriedades.length > 0)
        {
            for (String propriedade : propriedades)
            {
                try
                {
                    Object valor = PropertyUtils.getProperty(entidade, propriedade);
                    if (valor != null)
                    {
                        Predicate predicate = null;
                        if (isValidValueString(valor))
                        {
                            predicate = builder.like(root.<String>get(propriedade), "%" + (String) valor + "%");
                            predicates.add(predicate);
                        }
                        else if (isNotValueString(valor))
                        {
                            System.out.println(valor);
                            predicate = builder.equal(root.get(propriedade), valor);
                            predicates.add(predicate);
                        }
                    }
                }
                catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e)
                {
                    throw new RuntimeException(e);
                }
            }
        }

        criteria.where(predicates.toArray(new Predicate[0]));
        Query query = em.createQuery(criteria);

        return query.getResultList();
    }

    public List<T> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        TypedQuery<T> readQuery = em.createNamedQuery(classeEntidade.getSimpleName() + ".findAllConstructor", classeEntidade);
        return readQuery.setFirstResult(primeiroResultado)
                .setMaxResults(quantidadeDeResultados)
                .getResultList();
    }

    public Long getUltimoNumeroDeEntidade()
    {
        BigInteger ultimo = getEntityManager().createNamedQuery(classeEntidade.getSimpleName() + ".findLastNumber", BigInteger.class)
                                              .getSingleResult();
        if (ultimo == null)
        {
            ultimo = BigInteger.ONE;
        }
        return ultimo.longValue();
    }

    public Long buscaTotalDeItens()
    {
        TypedQuery<Long> readQuery = em.createQuery("SELECT COUNT(o) FROM " + classeEntidade.getSimpleName() + " o", Long.class);
        Long quantidade = readQuery.getSingleResult();
        if (quantidade == null)
        {
            quantidade = 0L;
        }
        return quantidade;
    }

    private boolean isValidValueString(Object valor)
    {
        return valor instanceof String && !((String) valor).trim().equals("");
    }

    private boolean isNotValueString(Object valor)
    {
        return !(valor instanceof String);
    }

    public abstract List<T> listaTodos();

    private boolean isNotNull(T entidade)
    {
        return (entidade != null);
    }

    protected EntityManager getEntityManager()
    {
        return em;
    }
    
    protected Class<T> getClasseEntidade()
    {
        return classeEntidade;
    }

    private void inicia()
    {
        if (em == null || !em.isOpen())
        {
            em = JPAUtil.getEntityManager();
        }
    }
}
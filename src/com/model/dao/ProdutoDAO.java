package com.model.dao;

import com.model.po.Produto;
import java.util.List;
import javax.persistence.EntityManager;

public class ProdutoDAO extends BorderoDAO<Produto>
{
    public ProdutoDAO(EntityManager em)
    {
        super(em, Produto.class);
    }

    @Override
    public List<Produto> listaTodos()
    {
        return getEntityManager().createNamedQuery("Produto.findAll")
                                 .getResultList();
    }
}
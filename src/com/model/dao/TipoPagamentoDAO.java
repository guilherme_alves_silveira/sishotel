package com.model.dao;

import com.model.po.TipoPagamento;
import java.util.List;
import javax.persistence.EntityManager;

public class TipoPagamentoDAO extends GenericoDAO<TipoPagamento, Short>
{
    public TipoPagamentoDAO(EntityManager em)
    {
        super(em, TipoPagamento.class);
    }

    @Override
    public List<TipoPagamento> listaTodos()
    {
        return getEntityManager().createNamedQuery("TipoPagamento.findAll")
                                 .getResultList();
    }
}
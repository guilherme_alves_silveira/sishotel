package com.model.dao;

import com.model.po.Diaria;
import java.util.List;
import javax.persistence.EntityManager;

public class DiariaDAO extends GenericoDAO<Diaria, Long>
{
    public DiariaDAO(EntityManager em)
    {
        super(em, Diaria.class);
    }

    @Override
    public List<Diaria> listaTodos()
    {
        return getEntityManager().createNamedQuery("Diaria.findAll")
                                 .getResultList();
    }
}
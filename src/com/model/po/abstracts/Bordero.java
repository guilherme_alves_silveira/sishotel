package com.model.po.abstracts;

import com.model.base.Entidade;
import com.model.po.Categoria;
import com.model.po.enumerated.TipoConsumo;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "bordero")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING,
                     name = "tipo_bordero")
public abstract class Bordero extends Entidade<Long>
{
    @Column(name = "descricao",
            nullable = false,
            length = 50)
    protected String descricao;

    @Column(name = "valor",
            nullable = false,
            precision = 8)
    protected BigDecimal valor;

    @Column(name = "tipo_consumo")
    @Enumerated(EnumType.STRING)
    protected TipoConsumo tipoConsumo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_categoria",
                referencedColumnName = "id",
                nullable = false)
    protected Categoria categoria;

    public Bordero()
    {
        //Construtor padão
    }

    public Bordero(Long id, TipoConsumo tipoConsumo)
    {
        this.setId(id);
        this.setTipoConsumo(tipoConsumo);
    }

    public Bordero(String id, String descricao, TipoConsumo tipoConsumo)
    {
        this.setId(Long.parseLong(id));
        this.setDescricao(descricao);
        this.setTipoConsumo(tipoConsumo);
    }

    @NotBlank(message = "descrição")
    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    @DecimalMin(value = "0",
                inclusive = true,
                message = "valor inválido")
    public BigDecimal getValor()
    {
        return valor;
    }

    public void setValor(BigDecimal valor)
    {
        this.valor = valor;
    }

    public TipoConsumo getTipoConsumo()
    {
        return tipoConsumo;
    }

    public void setTipoConsumo(TipoConsumo tipoConsumo)
    {
        this.tipoConsumo = tipoConsumo;
    }

    public String getDescricaoTipoConsumo()
    {
        return tipoConsumo.getDescricao();
    }

    @NotNull(message = "Escolha a categoria.")
    public Categoria getCategoria()
    {
        return categoria;
    }

    public void setCategoria(Categoria categoria)
    {
        this.categoria = categoria;
    }

    @Override
    public String toString()
    {
        return "Bordero{" + "id=" + id  + ",descricao=" + descricao + ", valor=" + valor +
               ", tipoConsumo=" + (tipoConsumo != null?tipoConsumo.getDescricao() : "null") +
               ", categoria=" + categoria + '}';
    }
}
package com.model.po.enumerated;

public enum TipoQuarto
{
    SOLTEIRO("Solteiro"),
    CASAL("Casal"),
    FAMILIA("Família");

    private final String descricao;

    TipoQuarto(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}

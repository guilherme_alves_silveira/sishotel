package com.model.po.enumerated;

public enum TipoConsumo
{
    PRODUTO("Produto", "PRODUTO"),
    SERVICO("Serviço", "SERVICO"),
    DESPESA("Despesa", "DESPESA");
    
    private final String descricao;
    private final String descricaoConsumo;
    
    TipoConsumo(String descricao, String descricaoConsumo)
    {
        this.descricao = descricao;
        this.descricaoConsumo = descricaoConsumo;
    }

    public String getDescricao()
    {
        return descricao;
    }
    
    public String getDescricaoConsumo()
    {
        return descricaoConsumo;
    }

    @Override
    public String toString()
    {
        return descricaoConsumo;
    }
}
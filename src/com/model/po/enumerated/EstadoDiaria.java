package com.model.po.enumerated;

public enum EstadoDiaria
{
    FECHADO("Fechado"),
    ABERTO("Aberto");

    private final String descricao;

    EstadoDiaria(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
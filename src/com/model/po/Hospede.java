package com.model.po;

import com.model.po.abstracts.Pessoa;
import com.model.vo.TrataLista;
import com.model.vo.TrataListaSet;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "hospede")
@NamedQueries(
        {
            @NamedQuery(name = "Hospede.findHospedeComDependentesById",
                        query = "SELECT h FROM Hospede h "
                        + "LEFT JOIN FETCH h.dependentes d WHERE h.id = :id"),
            @NamedQuery(name = "Hospede.findAll",
                        query = "SELECT h FROM Hospede h"),
            @NamedQuery(name = "Hospede.findLastNumber",
                        query = "SELECT MAX(h.id) + 1 FROM Hospede h"),
            @NamedQuery(name = "Hospede.findAllConstructor", query = "SELECT NEW com.model.po.Hospede(h.id, h.nome, h.cpf, h.passaporte) FROM Hospede h")
        })
public class Hospede extends Pessoa<EnderecoHospede>
{
    private transient final TrataLista<Dependente, Set> lista;
    @ManyToMany(mappedBy = "hospedes")
    private Set<Dependente> dependentes;

    @Column(name = "extras",
            precision = 10,
            scale = 8)
    private BigDecimal extras;

    @OneToOne(fetch = FetchType.LAZY,
              cascade = CascadeType.ALL,
              mappedBy = "hospede")
    private EnderecoHospede endereco;

    public Hospede()
    {
        this.dependentes = new HashSet<>();
        this.lista = new TrataListaSet(dependentes);
        extras = BigDecimal.ZERO;
    }

    public Hospede(BigDecimal extras)
    {
        this();
        this.extras = extras;
    }
    
    public Hospede(String id, String nome, String cpf, String passaporte)
    {
        this();
        this.setId(Long.parseUnsignedLong(id));
        this.setNome(nome);
        this.setCpf(cpf);
        this.setPassaporte(passaporte);
    }

    public Hospede(Long id)
    {
        this();
        this.setId(id);
    }

    @NotNull(message = "Informe o endereço.")
    @Override
    public EnderecoHospede getEndereco()
    {
        return endereco;
    }

    public void setEndereco(EnderecoHospede endereco)
    {
        this.endereco = endereco;
    }

    public void adicionaDependente(Dependente dependente)
    {
        lista.adiciona(dependente, "As informações do dependente inserido não podem estar vázias.");
    }

    public Dependente getDependente(Long id)
    {
        return lista.get(id, "O código do dependente não deve ser negativo.");
    }

    public void removeDependente(Dependente dependente)
    {
        lista.remove(dependente,
                     "O dependente informado deve estar com as informações preenchidas.",
                     "Este dependente não está associado com o este Hospede.");
    }

    public Set<Dependente> getDependentes()
    {
        return lista.getObjectos();
    }

    public void setDependentes(Set<Dependente> dependentes)
    {
        this.dependentes = dependentes;
    }

    public BigDecimal getExtras()
    {
        return extras;
    }

    public void setExtras(BigDecimal extras)
    {
        if (extras.doubleValue() < 0)
        {
            throw new IllegalArgumentException("O valor dos extras, não pode ser menor que zero.");
        }
        this.extras = extras;
    }
}
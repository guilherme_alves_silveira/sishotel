package com.model.po;

import com.model.po.abstracts.Pessoa;
import com.model.vo.TrataLista;
import com.model.vo.TrataListaSet;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "dependente")
@NamedQueries(
{
    @NamedQuery(name = "Dependente.findDependenteComHospedesById",
                query = "SELECT d FROM Dependente d "
                + "LEFT JOIN FETCH d.hospedes h WHERE d.id = :id"),
    @NamedQuery(name = "Dependente.findAll",
                query = "SELECT d FROM Dependente d"),
    @NamedQuery(name = "Dependente.findLastNumber",
                query = "SELECT MAX(d.id) + 1 FROM Dependente d"),
    @NamedQuery(name = "Dependente.findAllConstructor", query = "SELECT NEW com.model.po.Dependente(d.id, d.nome, d.cpf, d.passaporte) FROM Dependente d")
})
public class Dependente extends Pessoa<EnderecoDependente>
{
    private final transient TrataLista<Hospede, Set> lista;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dependente_de_hospede",
               joinColumns = @JoinColumn(name = "id_dependente"),
               inverseJoinColumns = @JoinColumn(name = "id_hospede"))
    private Set<Hospede> hospedes;

    @Column(name = "grau_parentesco",
            length = 50,
            nullable = false)
    private String grauParentesco;

    @OneToOne(fetch = FetchType.LAZY,
              cascade = CascadeType.ALL,
              mappedBy = "dependente")
    private EnderecoDependente endereco;

    public Dependente()
    {
        this.hospedes = new HashSet<>();
        this.lista = new TrataListaSet(hospedes);
    }

    public Dependente(Long id)
    {
        this();
        this.setId(id);
    }

    public Dependente(String id, String nome, String cpf, String passaporte)
    {
        this();
        this.setId(Long.parseUnsignedLong(id));
        this.setNome(nome);
        this.setCpf(cpf);
        this.setPassaporte(passaporte);
    }
    
    @NotNull(message = "Informe o endereço.")
    @Override
    public EnderecoDependente getEndereco()
    {
        return endereco;
    }

    public void setEndereco(EnderecoDependente endereco)
    {
        this.endereco = endereco;
    }

    @NotNull(message = "O dependente deve estar associado com o Hospede.")
    public Set<Hospede> getHospedes()
    {
        return lista.getObjectos();
    }

    public void adicionaHospede(Hospede hospede)
    {
        lista.adiciona(hospede, "As informações do Hospede inserido não podem estar vázias.");
    }

    public Hospede getHospede(Long id)
    {
        return lista.get(id, "O código do hospede não deve ser negativo.");
    }

    public void setHospedes(Set<Hospede> hospedes)
    {
        this.hospedes = hospedes;
    }

    public void removeHospede(Hospede hospede)
    {
        lista.remove(hospede,
                     "O hospede informado deve estar com as informações preenchidas.",
                     "Este hospede não está associado com o este Dependente.");
    }

    @NotBlank(message = "Informe o grau de parentesco.")
    public String getGrauParentesco()
    {
        return grauParentesco;
    }

    public void setGrauParentesco(String grauParentesco)
    {
        this.grauParentesco = grauParentesco;
    }
}
package com.model.po;

import com.model.po.enumerated.TipoQuarto;
import com.model.base.Entidade;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "quarto")
public class Quarto extends Entidade<Short> implements Serializable
{
    @Column(name = "numero",
            nullable = false)
    private Integer numero;
    
    @Column(name = "valor",
            precision = 10,
            scale = 8,
            nullable = false)
    private BigDecimal valor;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_quarto", 
            nullable = false)
    private TipoQuarto tipoQuarto;

    public int getNumero()
    {
        return numero;
    }

    public void setNumero(int numero)
    {
        this.numero = numero;
    }

    public BigDecimal getValor()
    {
        return valor;
    }

    public void setValor(BigDecimal valor)
    {
        this.valor = valor;
    }

    public TipoQuarto getTipoQuarto()
    {
        return tipoQuarto;
    }

    public void setTipoQuarto(TipoQuarto tipoQuarto)
    {
        this.tipoQuarto = tipoQuarto;
    }
}
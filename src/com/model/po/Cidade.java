package com.model.po;

import com.model.base.Entidade;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "cidade")
@NamedQueries
({
    @NamedQuery(name = "Cidade.findAllConstructor", 
                query = "SELECT NEW com.model.po.Cidade(c.id, c.nome) FROM Cidade c"),
    @NamedQuery(name = "Cidade.findAllConstructorByUF", 
                query = "SELECT NEW com.model.po.Cidade(c.id, c.nome) FROM Cidade c WHERE c.estado.uf = :uf")
})
public class Cidade extends Entidade<Integer>
{
    @Column(name = "nome", 
            length = 50, 
            nullable = false)
    private String nome;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_estado",
                referencedColumnName = "id",
                nullable = false)
    private Estado estado;
    
    @Column(name = "codigo",
            length = 10,
            nullable = false)
    private String codigo;
    
    public Cidade()
    {
        //Construtor default
    }
    
    public Cidade(Integer id, String nome)
    {
        this.setId(id);
        this.setNome(nome);
    }
    
    @NotBlank
    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @NotNull
    public Estado getEstado()
    {
        return estado;
    }

    public void setEstado(Estado estado)
    {
        this.estado = estado;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    @Override
    public String toString()
    {
        return getNome();
    }
}
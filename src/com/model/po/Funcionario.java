package com.model.po;

import com.model.po.abstracts.Pessoa;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "funcionario")
@NamedQueries(
        {
            @NamedQuery(name = "Funcionario.findFuncionarioComEnderecoById",
                        query = "SELECT f FROM Funcionario f "
                        + "JOIN f.endereco e WHERE f.id = :id"),
            @NamedQuery(name = "Funcionario.findAll",
                        query = "SELECT f FROM Funcionario f"),
            @NamedQuery(name = "Funcionario.findLastNumber",
                        query = "SELECT MAX(f.id) + 1 FROM Funcionario f"),
            @NamedQuery(name = "Funcionario.findAllConstructor", 
                        query = "SELECT NEW com.model.po.Funcionario(f.id, f.nome, f.cpf, f.rg) FROM Funcionario f")
        })
public class Funcionario extends Pessoa<EnderecoFuncionario>
{
    @Column(name = "salario",
            precision = 10,
            scale = 8)
    private BigDecimal salario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcao",
                nullable = false)
    private Funcao funcao;

    @Column(name = "cargo",
            length = 50)
    private String cargo;

    @Column(name = "hora_entrada")
    private Integer horaEntrada;

    @Column(name = "hora_saida")
    private Integer horaSaida;

    @Column(name = "login",
            length = 50)
    private String login;

    @Column(name = "senha",
            length = 50)
    private String senha;

    @OneToOne(fetch = FetchType.LAZY,
              cascade = CascadeType.ALL,
              mappedBy = "funcionario")
    private EnderecoFuncionario endereco;

    public Funcionario()
    {
        //Construtor padrão
    }

    public Funcionario(Long id)
    {
        this.setId(id);
    }
    
    public Funcionario(String id, String nome, String cpf, String rg)
    {
        this(Long.parseLong(id));
        this.setNome(nome);
        this.setCpf(cpf);
        this.setRg(rg);
    }
    
    public Funcionario(String login, String senha)
    {
        this.login = login;
        this.senha = senha;
    }

    public Funcionario(BigDecimal salario, Funcao funcao,
                       String cargo, Integer horaEntrada,
                       Integer horaSaida)
    {
        this.salario = salario;
        this.funcao = funcao;
        this.cargo = cargo;
        this.horaEntrada = horaEntrada;
        this.horaSaida = horaSaida;
    }

    public Funcionario(BigDecimal salario, Funcao funcao,
                       String cargo, Integer horaEntrada,
                       Integer horaSaida, String login,
                       String senha)
    {
        this(login, senha);
        this.salario = salario;
        this.funcao = funcao;
        this.cargo = cargo;
        this.horaEntrada = horaEntrada;
        this.horaSaida = horaSaida;
    }

    @NotNull(message = "Informe o endereço.")
    @Override
    public EnderecoFuncionario getEndereco()
    {
        return endereco;
    }

    public void setEndereco(EnderecoFuncionario endereco)
    {
        this.endereco = endereco;
    }

    public BigDecimal getSalario()
    {
        return salario;
    }

    public void setSalario(BigDecimal salario)
    {
        if (salario.doubleValue() < 0)
        {
            throw new IllegalArgumentException(
                    "O salário não pode ser menor que zero.");
        }
        else
        {
            this.salario = salario;
        }
    }

    public Funcao getFuncao()
    {
        return funcao;
    }

    public void setFuncao(Funcao funcao)
    {
        this.funcao = funcao;
    }

    public String getCargo()
    {
        return cargo;
    }

    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

    public int getHoraEntrada()
    {
        return horaEntrada;
    }

    public void setHoraEntrada(int horaEntrada)
    {
        this.horaEntrada = horaEntrada;
    }

    public int getHoraSaida()
    {
        return horaSaida;
    }

    public void setHoraSaida(int horaSaida)
    {
        this.horaSaida = horaSaida;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getSenha()
    {
        return senha;
    }

    public void setSenha(String senha)
    {
        this.senha = senha;
    }
}
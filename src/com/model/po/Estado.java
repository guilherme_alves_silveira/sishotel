package com.model.po;

import com.model.base.Entidade;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@Table(name = "estado")
@Cache(type = CacheType.FULL)
@ReadOnly
@NamedQuery(name = "Estado.findAllConstructor", 
            query = "SELECT NEW com.model.po.Estado(e.id, e.uf) FROM Estado e")
public class Estado extends Entidade<Short>
{
    @Column(name = "uf", 
            length = 2,
            nullable = false)
    private String uf;
    
    @Column(name = "nome", 
            length = 50,
            nullable = false)
    private String nome;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pais", 
                referencedColumnName = "id", 
                nullable = true)
    private Pais pais;
    
    public Estado()
    {
        //Construtor default
    }
    
    public Estado(Short id, String uf)
    {
        this.setId(id);
        this.setUf(uf);
    }
    
    public String getUf()
    {
        return uf;
    }

    public void setUf(String uf)
    {
        this.uf = uf;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Pais getPais()
    {
        return pais;
    }
    
    public String getNomePais()
    {
        return pais.getNome();
    }

    public void setPais(Pais pais)
    {
        this.pais = pais;
    }

    @Override
    public String toString()
    {
        return getUf();
    }
}
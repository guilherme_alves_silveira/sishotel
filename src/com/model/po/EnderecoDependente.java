package com.model.po;

import com.model.po.abstracts.Endereco;
import com.model.po.abstracts.Pessoa;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "endereco_dependente")
public class EnderecoDependente extends Endereco<Dependente>
{
    @OneToOne(fetch = FetchType.EAGER,
              cascade = CascadeType.ALL)
    @JoinColumn(name = "id_dependente",
                referencedColumnName = "id",
                nullable = false)
    private Dependente dependente;

    public Dependente getDependente()
    {
        return dependente;
    }

    public void setDependente(Dependente dependente)
    {
        this.dependente = dependente;
        this.dependente.setEndereco(this);
    }

    @Override
    public void setPessoa(Dependente dependente)
    {
        setDependente(dependente);
    }
}
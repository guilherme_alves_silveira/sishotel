package com.model.vo;

/**
 *
 * @author A-IKASORUK
 * @param <T> Tipo do Objeto a ser retornado
 * @param <L> Tipo da Lista
 */
public interface TrataLista<T, L>
{
    public L getObjectos();

    public void adiciona(T objeto, String mensagem);

    public T get(Long id, String mensagem);

    public void remove(T objeto, String mensagem1, String mensagem2);
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model.vo.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author A-IKASORUK
 */
public final class TempoUtil
{
    public synchronized static LocalDateTime converteCalendarParaLocalDateTime(Calendar calendario)
    {
        if (calendario == null)
        {
            throw new NullPointerException("A data deve ser instânciada.");
        }

        Date data = calendario.getTime();
        return LocalDateTime.ofInstant(data.toInstant(),
                                       ZoneId.systemDefault());
    }

    public static LocalDate converteCalendarParaLocalDate(Calendar calendario)
    {
        return converteCalendarParaLocalDateTime(calendario).toLocalDate();
    }
}
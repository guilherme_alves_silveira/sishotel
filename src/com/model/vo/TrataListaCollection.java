package com.model.vo;

import com.model.base.Entidade;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

public class TrataListaCollection<T extends Entidade> implements TrataLista<T, Collection>
{
    private final Collection<T> objetos;
    
    public TrataListaCollection(Collection<T> objetos)
    {
        this.objetos = objetos;
    }
    
    @Override
    public Collection getObjectos()
    {
        return Collections.unmodifiableCollection(objetos);
    }

    @Override
    public void adiciona(T objeto, String mensagem)
    {
        Objects.requireNonNull(objeto, mensagem);
        this.objetos.add(objeto);
    }

    @Override
    public T get(Long id, String mensagem)
    {
        if (id < 0)
        {
            throw new IllegalArgumentException(mensagem);
        }
        
        for (T objeto : objetos)
        {
            if(Objects.equals(objeto.getId(), id))
            {
                return objeto;
            }
        }
        
        return null;
    }

    @Override
    public void remove(T objeto, String mensagem1, String mensagem2)
    {
        Objects.requireNonNull(objeto, mensagem1);

        if (this.objetos.contains(objeto))
        {
            this.objetos.remove(objeto);
        }
        else
        {
            throw new IllegalArgumentException(mensagem2);
        }
    }
}
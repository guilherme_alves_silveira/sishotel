package com.model.vo;

import com.model.po.Diaria;
import com.model.vo.util.TempoUtil;
import com.util.SistemaUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

public class CalculadoraDiaria
{
    private static final BigDecimal HORAS_PARA_CALCULO = SistemaUtils.horasParaCalculoDaDiaria();
    //Data de entrada e saida que serão 
    //utilizados no calculo da diária
    private LocalDateTime dataEntradaCalc;
    private LocalDateTime dataSaidaCalc;
    private BigDecimal valorDiaria;
    private BigDecimal valorQuarto;
    private BigDecimal valorHoras;

    public void calcula(Diaria diaria)
    {
        if(diaria == null)
        {
            throw new NullPointerException("A diária não deve ser nula.");
        }
        
        this.dataEntradaCalc = converteCalendarParaLocalDateTime(diaria.getDataEntrada());
        this.dataSaidaCalc = converteCalendarParaLocalDateTime(diaria.getDataSaida());
        //O valor do quarto influência na diária
        this.valorQuarto = diaria.getQuarto().getValor();
        //Se a data de entrada for antes da data de saída
        if (dataEntradaCalc.isBefore(dataSaidaCalc))
        {
            //Recebe o número de horas de diferença entre a data inicial e a data final
            BigDecimal diferencaDeHoras = new BigDecimal(ChronoUnit.HOURS.between(dataEntradaCalc, dataSaidaCalc));

            if (diferencaDeHoras.intValue() > HORAS_PARA_CALCULO.intValue())
            {
                //valor = (valor do quarto) / 12 ou 24 ou o que estiver configurado no sistema
                try
                {
                    this.valorHoras = valorQuarto.divide(HORAS_PARA_CALCULO);
                }
                catch (ArithmeticException e)
                {
                    this.valorHoras = valorQuarto.divide(HORAS_PARA_CALCULO, 12, RoundingMode.CEILING);
                }
                //diaria = valor * (diferença de horas)
                this.valorDiaria = valorHoras.multiply(diferencaDeHoras);
            }
            else
            {
                //diaria = (valor do quarto)
                this.valorDiaria = valorQuarto;
            }
        }
        else // Se não, é lançada uma exceção.
        {
            throw new IllegalArgumentException("A data de entrada é depois da data de saída.\n Verifique se o relógio de seu sistema está correto.");
        }

        diaria.setValor(valorDiaria);
        resetaValores();
    }

    private LocalDateTime converteCalendarParaLocalDateTime(Calendar calendario)
    {
        return TempoUtil.converteCalendarParaLocalDateTime(calendario);
    }

    private void resetaValores()
    {
        this.valorDiaria = BigDecimal.ZERO;
        this.valorQuarto = BigDecimal.ZERO;
        this.valorHoras = BigDecimal.ZERO;
    }
}
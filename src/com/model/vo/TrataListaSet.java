package com.model.vo;

import com.model.base.Entidade;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class TrataListaSet<T extends Entidade> implements TrataLista<T, Set>
{
    private final Set<T> objetos;
    
    public TrataListaSet(Set<T> objetos)
    {
        this.objetos = objetos;
    }
    
    @Override
    public Set getObjectos()
    {
        return Collections.unmodifiableSet(objetos);
    }

    @Override
    public void adiciona(T objeto, String mensagem)
    {
        Objects.requireNonNull(objeto, mensagem);
        this.objetos.add(objeto);
    }

    @Override
    public T get(Long id, String mensagem)
    {
        if (id < 0)
        {
            throw new IllegalArgumentException(mensagem);
        }
        
        for (T objeto : objetos)
        {
            if(Objects.equals(objeto.getId(), id))
            {
                return objeto;
            }
        }
        
        return null;
    }

    @Override
    public void remove(T objeto, String mensagem1, String mensagem2)
    {
        Objects.requireNonNull(objeto, mensagem1);

        if (this.objetos.contains(objeto))
        {
            this.objetos.remove(objeto);
        }
        else
        {
            throw new IllegalArgumentException(mensagem2);
        }
    }
}
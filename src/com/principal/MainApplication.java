package com.principal;

import com.controller.MainController;
import com.util.JPAUtil;
import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.Window;
import javax.persistence.PersistenceException;
import javax.swing.JOptionPane;
import jfxtras.labs.scene.layout.ScalableContentPane;

public class MainApplication extends Application 
{
    private static final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    private final ScalableContentPane scaledPane = new ScalableContentPane();
    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception
    {
        try
        {
            MainApplication.stage = stage;
            configuraScaledPane();
            //O root é utilizado para adicionar os nodes
            Pane root = scaledPane.getContentPane();
            MainController mainController = new MainController(root);
            Scene scene = new Scene(scaledPane);
            stage.setScene(scene);
            stage.setFullScreen(true);
            stage.setTitle("SisHotel - Gerenciamento de Hotéis");
            stage.show();
            new Thread(() ->
            {
                JPAUtil.getEntityManager();
                JPAUtil.closeEntityManager();
            }).start();
        }
        catch (PersistenceException e)
        {
            JOptionPane.showMessageDialog(null, "Impossibilidade de se conectar com o banco de dados.\n"
                                          + "1 - Verifique se o servidor está ligado.\n"
                                          + "2 - Verifique se o antivirus está bloqueando a aplicação.\n"
                                          + "3 - Verifique se o firewall está bloqueando o acesso ao banco de dados.");
            System.exit(0);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null,
                                          "Ocorrência detectada. Contate o administrador.:" + e.getMessage());
        }
    }

    private void configuraScaledPane()
    {
        scaledPane.setMinSize(800, 600);
        scaledPane.setMaxSize(dim.getWidth(), dim.getHeight());
        scaledPane.setAutoRescale(true);
        scaledPane.setAspectScale(true);
    }
    
    public static Dimension getDimension()
    {
        return dim;
    }
    
    public static Window getStage()
    {
        return stage;
    }
    
    public static void main(String[] args)
    {
        launch(args);
    }
}
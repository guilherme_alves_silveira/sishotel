package com.util;

import java.math.BigDecimal;

public class SistemaUtils
{
    /**
     * Classe centralizada que determina o máximo de desconto permitido
     * @param valor Valor a ser passado
     * @return BigDecimal
     */
    public static synchronized BigDecimal valorPermitidoPeloSistemaDeAcordoComPorcentagem(BigDecimal valor)
    {
        return valor;
    }

    public static synchronized BigDecimal horasParaCalculoDaDiaria()
    {
        return new BigDecimal(12);
    }
}
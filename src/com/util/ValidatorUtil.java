package com.util;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Classe utilizada para validar os Beans.
 * @author A-IKASORUK
 */
public class ValidatorUtil
{
    private static final ValidatorFactory factory;
    
    static
    {
        factory = Validation.buildDefaultValidatorFactory();
    }
    
    private ValidatorUtil()
    {
        //Construtor privado pois está classe segue o padrão Singleton e não deve ser instânciada
    }
    
    /**
     * @return Validator
     */
    public static Validator getValidator()
    {
        return factory.getValidator();
    }
}
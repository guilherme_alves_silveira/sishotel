package com.util;

import com.model.base.Entidade;
import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

/**
 * Classe utilizada para facilitar o uso do Validator
 * @author A-IKASORUK
 * @param <T> Classe a ser utilizada.
 */
public class ValidatorHelper<T extends Entidade>
{

    private Set<ConstraintViolation<T>> restricao;
    private final Validator validador;

    public ValidatorHelper()
    {
        this.validador = ValidatorUtil.getValidator();
    }

    /**
     * Utilizado para validar a entidade.
     *
     * @param entidade
     *
     * @return Retorna a própria classe, para que tenha uma interface fluente.
     */
    public ValidatorHelper valida(T entidade)
    {
        this.restricao = this.validador.validate(entidade);
        return this;
    }

    public ValidatorHelper validaPropriedade(T entidade, String propriedate)
    {
        this.restricao = this.validador.validateProperty(entidade,
                                                         propriedate,
                                                         (Class<?>) null);
        return this;
    }

    public String getMessage()
    {
        if (this.restricao != null && ouveViolacaoDeRestricao())
        {
            return this.restricao.iterator()
                    .next()
                    .getMessage();
        }

        return "";
    }

    public Iterator<ConstraintViolation<T>> getMessages()
    {
        return this.restricao.iterator();
    }

    /**
     * Utilizado somente para debug
     */
    public void imprimeMessages()
    {
        if (this.restricao != null)
        {
            this.restricao.forEach(i -> System.out.println(i.getMessage()));
        }
    }

    public boolean ouveViolacaoDeRestricao()
    {
        if (this.restricao != null)
        {
            return restricao.size() > 0;
        }
        else
        {
            throw new NullPointerException("A restrição está nula.");
        }
    }

    public Integer quantidadeDeViolacoes()
    {
        if (this.restricao != null)
        {
            return restricao.size();
        }
        else
        {
            return 0;
        }
    }
}

package com.util;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceUnitTransactionType;
import static org.eclipse.persistence.config.PersistenceUnitProperties.TRANSACTION_TYPE;

public final class JPAUtil
{
    private static final String PERSISTENCE_UNIT = "SisHotelPU";
    private static final EntityManagerFactory factory;
    private static final ThreadLocal<EntityManager> tManager;
    private static final Map<String, Object> properties;

    static
    {
        properties = new HashMap<>();
        configuraProperties();
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT, properties);
        tManager = new ThreadLocal<>();
    }

    private static void configuraProperties()
    {
        properties.put(TRANSACTION_TYPE, PersistenceUnitTransactionType.RESOURCE_LOCAL.name());
        properties.put("javax.persistence.jdbc.url", "jdbc:mysql://localhost:3306/sis_hotel");
        properties.put("javax.persistence.jdbc.user", "root");
        properties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
        properties.put("javax.persistence.jdbc.password", "");
    }

    private JPAUtil()
    {
        //Construtor privado pois está classe segue o padrão Singleton
    }

    /**
     * @return uma EntityManager para que seja realizada a conexão com o banco
     * de dados
     */
    public static EntityManager getEntityManager()
    {
        EntityManager em = tManager.get();

        if (em == null || !em.isOpen())
        {
            em = factory.createEntityManager();
            JPAUtil.tManager.set(em);
        }

        return tManager.get();
    }

    /**
     * Fecha o EntityManager.
     */
    public static void closeEntityManager()
    {
        EntityManager em = tManager.get();

        if (em != null)
        {
            EntityTransaction transaction = em.getTransaction();

            if (transaction.isActive())
            {
                transaction.rollback();
            }
            em.close();
            JPAUtil.tManager.set(null);
        }
    }

    /**
     * Fecha a fabrica de EntityManager. Obs.: A conexão com o banco de dados é
     * fechada.
     */
    public static void closeEntityManagerFactory()
    {
        try
        {
            closeEntityManager();
            factory.close();
        }
        catch (Exception e)
        {
            System.out.println("ERRO AO FECHAR CONEXÃO: " + e.getMessage());
        }
    }

    /**
     * Pode ser utilizado para questões em que seja necessário uma melhor
     * performance.
     *
     * @return Conexão com o banco de dados (JDBC)
     */
    public static Connection getConnection()
    {
        EntityManager em = tManager.get();
        if (em == null)
        {
            em = getEntityManager();
        }
        em.getTransaction().begin();
        Connection con = em.unwrap(Connection.class);
        em.getTransaction().commit();
        return con;
    }
}
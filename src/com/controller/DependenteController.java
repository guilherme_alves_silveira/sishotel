package com.controller;

import com.controller.abstracts.PessoaController;
import com.model.dao.DependenteDAO;
import com.model.po.Dependente;
import com.model.po.EnderecoDependente;
import java.util.List;

public class DependenteController  extends PessoaController<Dependente, EnderecoDependente>
{
    private DependenteDAO dao;

    @Override
    public void salvaOuAtualiza(Dependente dependente)
    {
        try
        {
            validador.valida(dependente);
            validadorEndereco.valida(dependente.getEndereco());
            if (!validador.ouveViolacaoDeRestricao())
            {
                try
                {
                    iniciaTransacao();
                    dao = new DependenteDAO(getEntityManager());
                    begin();
                    dao.salvaOuAtualiza(dependente);
                    commit();
                }
                catch (Exception e)
                {
                    executaRollback();
                }
                finally
                {
                    fechaConexao();
                }
            }
            else
            {
                configuraAlertaDeDadosPreenchidosIncorretamente();
                exibeAlertaComErros();
            }
        }
        catch (javax.el.PropertyNotFoundException e)
        {
            //Ignora pois é bug do próprio validator. Corrigido na versão alpha
        }
    }

    @Override
    public Dependente busca(Dependente dependente)
    {
        try
        {
            iniciaEntityManager();
            dao = new DependenteDAO(getEntityManager());
            return dao.pegaPorId(dependente.getId());
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public void deleta(Dependente dependente)
    {
        try
        {
            iniciaTransacao();
            dao = new DependenteDAO(getEntityManager());
            dependente = dao.pegaPorId(dependente.getId());
            EnderecoDependente endereco = getEntityManager().merge(dependente.getEndereco());
            dependente.setEndereco(endereco);
            begin();
            dao.deleta(dependente);
            commit();
        }
        catch (Exception e)
        {
            executaRollback();
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long getUltimoNumeroDeEntidade()
    {
        iniciaEntityManager();
        try
        {
            dao = new DependenteDAO(getEntityManager());
            Long numero = dao.getUltimoNumeroDeEntidade();
            return numero;
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Dependente> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        try
        {
            iniciaEntityManager();
            dao = new DependenteDAO(getEntityManager());
            return dao.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Dependente> buscaListaFiltrada(Dependente entidade, String[] propriedades)
    {
        try
        {
            iniciaEntityManager();
            dao = new DependenteDAO(getEntityManager());
            return dao.filtrar(entidade, propriedades);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long buscaTotalDeItens()
    {
        try
        {
            iniciaEntityManager();
            dao = new DependenteDAO(getEntityManager());
            return dao.buscaTotalDeItens();
        }
        finally
        {
            fechaConexao();
        }
    }
}
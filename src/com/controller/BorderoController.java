package com.controller;

import com.controller.abstracts.Controller;
import com.model.dao.BorderoDAO;
import com.model.dao.DespesaDAO;
import com.model.dao.ProdutoDAO;
import com.model.dao.ServicoDAO;
import com.model.po.abstracts.Bordero;
import com.model.po.enumerated.TipoConsumo;
import java.util.Iterator;
import java.util.List;
import javax.validation.ConstraintViolation;

public class BorderoController extends Controller<Bordero>
{
    private BorderoDAO dao;

    public BorderoController()
    {
        super();
    }

    @Override
    protected void exibeAlertaComErros()
    {
        Iterator<ConstraintViolation<Bordero>> it = validador.getMessages();
        while (it.hasNext())
        {
            String mensagem = it.next().getMessage();
            builder.append("*")
                   .append(mensagem)
                   .append("\n");
        }

        getMensagemErro().setContentText(builder.toString());
        //Esvazia o StringBuilder
        builder.setLength(0);
        getMensagemErro().show();
    }

    @Override
    public void salvaOuAtualiza(Bordero bordero)
    {
        try
        {
            validador.valida(bordero);
            if (!validador.ouveViolacaoDeRestricao())
            {
                try
                {
                    iniciaTransacao();
                    dao = instanciaDAOPorTipoDeConsumo(bordero.getTipoConsumo());
                    begin();
                    dao.salvaOuAtualiza(bordero);
                    commit();
                }
                catch (Exception e)
                {
                    executaRollback();
                }
                finally
                {
                    fechaConexao();
                }
            }
            else
            {
                configuraAlertaDeDadosPreenchidosIncorretamente();
                exibeAlertaComErros();
            }
        }
        catch (javax.el.PropertyNotFoundException e)
        {
            //Ignora pois é bug do próprio validator. Corrigido na versão alpha
        }
    }

    @Override
    public Bordero busca(Bordero bordero)
    {
        try
        {
            iniciaEntityManager();
            dao = instanciaDAOPorTipoDeConsumo(bordero.getTipoConsumo());
            return (Bordero) dao.pegaPorId(bordero.getId());
        }
        finally
        {
            fechaConexao();
        }
    }
    
    public Bordero busca(Long idBordero)
    {
        try
        {
            iniciaEntityManager();
            TipoConsumo tipoConsumo = BorderoDAO.getTipoConsumoBordero(idBordero);
            if(tipoConsumo != null)
            {
                dao = instanciaDAOPorTipoDeConsumo(tipoConsumo);
                Bordero bordero = (Bordero) dao.pegaPorId(idBordero);
                if(bordero != null)
                {
                    bordero.setTipoConsumo(tipoConsumo);
                }
                return bordero;
            }
            
            return null;
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public void deleta(Bordero bordero)
    {
        try
        {
            iniciaTransacao();
            dao = instanciaDAOPorTipoDeConsumo(bordero.getTipoConsumo());
            bordero = (Bordero) dao.pegaPorId(bordero.getId());
            begin();
            dao.deleta(bordero);
            commit();
        }
        catch (Exception e)
        {
            executaRollback();
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long getUltimoNumeroDeEntidade()
    {
        iniciaEntityManager();
        try
        {
            dao = new BorderoDAO(getEntityManager(), Bordero.class);
            Long numero = dao.getUltimoNumeroDeEntidade();
            return numero;
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Bordero> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        try
        {
            iniciaEntityManager();
            dao = new BorderoDAO(getEntityManager(), Bordero.class);
            return dao.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Bordero> buscaListaFiltrada(Bordero entidade, String[] propriedades)
    {
        try
        {
            iniciaEntityManager();
            dao = new BorderoDAO(getEntityManager(), Bordero.class);
            return dao.filtrar(entidade, propriedades);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long buscaTotalDeItens()
    {
        try
        {
            iniciaEntityManager();
            dao = new BorderoDAO(getEntityManager(), Bordero.class);
            return dao.buscaTotalDeItens();
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public boolean isValidadoComSucesso()
    {
        if (validador.ouveViolacaoDeRestricao())
        {
            return false;
        }

        return !validador.ouveViolacaoDeRestricao();
    }

    /**
     * Instância o DAO por tipo de bordero.
     * @param bordero 
     * @return DAO
     */
    private BorderoDAO instanciaDAOPorTipoDeConsumo(TipoConsumo tipoConsumo)
    {
        switch (tipoConsumo)
        {
            case PRODUTO:
                return new ProdutoDAO(getEntityManager());
            case SERVICO:
                return new ServicoDAO(getEntityManager());
            case DESPESA:
                return new DespesaDAO(getEntityManager());
            default:
                throw new IllegalArgumentException("Bordero Inválido!");
        }
    }
}
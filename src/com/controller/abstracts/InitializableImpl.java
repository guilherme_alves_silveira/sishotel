package com.controller.abstracts;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import jfxtras.labs.scene.control.window.CloseIcon;
import jfxtras.labs.scene.control.window.MinimizeIcon;
import jfxtras.labs.scene.control.window.Window;
import jfxtras.labs.scene.control.window.WindowIcon;

/**
 *
 * @author Guilherme A. Silveira
 */
public abstract class InitializableImpl implements Initializable
{
    private WindowIcon iconeMinimizador;
    private WindowIcon iconeFecha;
    private final Window window;

    public InitializableImpl()
    {
        this.window = new Window();
        this.window.setLayoutX(100);
        this.window.setLayoutY(100);
        this.window.setResizableWindow(false);
        iniciaIcones();
    }

    protected abstract void inicia();

    private void iniciaIcones()
    {
        this.iconeMinimizador = new MinimizeIcon(window);
        this.iconeFecha = new CloseIcon(window);
        this.window.getRightIcons().addAll(iconeMinimizador, iconeFecha);
        this.window.setResizableWindow(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //Utilizado caso for utilizar o fxmls
    }

    public Window getWindow()
    {
        return window;
    }
}
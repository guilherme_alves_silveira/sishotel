package com.controller.abstracts;

import com.model.po.abstracts.Endereco;
import com.model.po.abstracts.Pessoa;
import com.util.ValidatorHelper;
import java.util.Iterator;
import javax.validation.ConstraintViolation;

public abstract class PessoaController<T extends Pessoa, E extends Endereco> extends Controller<T>
{
    protected final ValidatorHelper<E> validadorEndereco;

    public PessoaController()
    {
        super();
        this.validadorEndereco = new ValidatorHelper<>();
    }

    @Override
    protected final void exibeAlertaComErros()
    {
        Iterator<ConstraintViolation<T>> it = validador.getMessages();
        while (it.hasNext())
        {
            String mensagem = it.next().getMessage();
            builder.append("*")
                    .append(mensagem)
                    .append("\n");
        }

        Iterator<ConstraintViolation<E>> ite = validadorEndereco.getMessages();
        while (ite.hasNext())
        {
            String mensagem = ite.next().getMessage();
            builder.append("*")
                    .append(mensagem)
                    .append("\n");
        }
        getMensagemErro().setContentText(builder.toString());
        //Esvazia o StringBuilder
        builder.setLength(0);
        getMensagemErro().show();
    }

    @Override
    public boolean isValidadoComSucesso()
    {
        if (validador.ouveViolacaoDeRestricao())
        {
            return false;
        }

        return !validadorEndereco.ouveViolacaoDeRestricao();
    }
}
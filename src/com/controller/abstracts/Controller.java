package com.controller.abstracts;

import com.model.base.Entidade;
import com.principal.MainApplication;
import com.util.JPAUtil;
import com.util.ValidatorHelper;
import java.util.List;
import javafx.animation.PauseTransition;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import javafx.util.Duration;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public abstract class Controller<T extends Entidade>
{
    protected final ValidatorHelper<T> validador;
    protected final StringBuilder builder;
    private Alert mensagemErro;
    private EntityManager em;
    private EntityTransaction tran;
    
    public Controller()
    {
        this.builder = new StringBuilder();
        this.validador = new ValidatorHelper<>();
    }

    protected void configuraAlertaDeDadosPreenchidosIncorretamente()
    {
        this.mensagemErro = new Alert(Alert.AlertType.ERROR);
        /**
         * Inicia uma transição, com um tempo de dez segundos.
         * Após passado os 10 segundos, a tela de mensagem é fechada.
         **/
        PauseTransition pausa = new PauseTransition(Duration.seconds(10));
        pausa.setOnFinished((f) -> 
        {
            mensagemErro.hide();
        });
        pausa.play();
        mensagemErro.initOwner(MainApplication.getStage());
        mensagemErro.initModality(Modality.WINDOW_MODAL);
    }
    
    protected EntityManager getEntityManager()
    {
        return em;
    }
    
    protected Alert getMensagemErro()
    {
        return mensagemErro;
    }

    protected void iniciaTransacao()
    {
        iniciaEntityManager();
        tran = em.getTransaction();
    }
    
    protected void iniciaEntityManager()
    {
        em = JPAUtil.getEntityManager();
    }

    protected void executaRollback()
    {
        if (tran != null && tran.isActive())
        {
            tran.rollback();
        }
    }

    protected void fechaConexao()
    {
        JPAUtil.closeEntityManager();
    }
    
    protected void begin()
    {
        tran.begin();
    }
    
    protected void commit()
    {
        tran.commit();
    }
    
    protected abstract void exibeAlertaComErros();
    public abstract void salvaOuAtualiza(T entidade);
    public abstract T busca(T entidade);
    public abstract void deleta(T entidade);
    public abstract Number getUltimoNumeroDeEntidade();
    public abstract boolean isValidadoComSucesso();
    public abstract List<T> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados);
    public abstract List<T> buscaListaFiltrada(T entidade, String[] propriedades);
    public abstract Number buscaTotalDeItens();
}
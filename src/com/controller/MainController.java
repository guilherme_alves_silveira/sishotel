package com.controller;

import com.principal.MainApplication;
import com.view.tabela.BorderoTabelaView;
import com.view.tabela.DependenteTabelaView;
import com.view.tabela.FuncionarioTabelaView;
import com.view.tabela.HospedeTabelaView;
import com.view.tabela.template.TabelaView;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.Pane;

public class MainController implements Initializable
{
    private MenuBar mnbMain;
    //****************************************
    private Menu mnCadastros;
    private Menu mnMovimentacoesGeraiss;
    private Menu mnConsultas;
    private Menu mnRelatorios;
    //****************************************
    private MenuItem mniCadastroHospede;
    private MenuItem mniCadastroDependente;
    private MenuItem mniCadastroBordero;
    private MenuItem mniCadastroFuncionario;
    private MenuItem mniCadastroFuncaoFuncionario;
    private MenuItem mniCadastroQuarto;
    private MenuItem mniCadastroReserva;
    private MenuItem mniCadastroTipoPagamento;
    //****************************************
    private final Pane root;

    public MainController(Pane root)
    {
        this.root = Objects.requireNonNull(root, "O ScalableContentPane não deve ser nulo.");
        inicia();
    }

    private void inicia()
    {
        iniciaMenuBar();
        iniciaMenus();
        iniciaMenuItems();
        iniciaEventos();
    }

    private void iniciaMenuBar()
    {
        mnbMain = new MenuBar();
        double width = MainApplication.getDimension().getWidth();
        mnbMain.setPrefWidth(width);
    }

    private void iniciaMenus()
    {
        mnCadastros = new Menu("Cadastros");
        mnMovimentacoesGeraiss = new Menu("Movimentações Gerais");
        mnConsultas = new Menu("Consultas");
        mnRelatorios = new Menu("Relatórios");
        mnbMain.getMenus().addAll(mnCadastros, mnMovimentacoesGeraiss,
                                  mnConsultas, mnRelatorios);
        root.getChildren().add(mnbMain);
    }

    private void iniciaMenuItems()
    {
        mniCadastroHospede = new MenuItem("Cadastro de Hospedes");
        mniCadastroDependente = new MenuItem("Cadastro de Dependente");
        mniCadastroBordero = new MenuItem("Cadastro de Bordero");
        mniCadastroFuncionario = new MenuItem("Cadastro de Funcionário");
        mniCadastroFuncaoFuncionario = new MenuItem("Cadastro de Funções de Funcionários");
        mniCadastroQuarto = new MenuItem("Cadastro de Quartos");
        mniCadastroReserva = new MenuItem("Cadastro de Reservas");
        mniCadastroTipoPagamento = new MenuItem("Cadastro de Tipos de Pagamento");
        mnCadastros.getItems().addAll(mniCadastroHospede,
                                      mniCadastroDependente,
                                      new SeparatorMenuItem(),
                                      mniCadastroBordero,
                                      new SeparatorMenuItem(),
                                      mniCadastroFuncionario,
                                      mniCadastroFuncaoFuncionario,
                                      new SeparatorMenuItem(),
                                      mniCadastroQuarto,
                                      mniCadastroReserva,
                                      new SeparatorMenuItem(),
                                      mniCadastroTipoPagamento);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //Utilizado caso for trabalhar coms o fxmls
    }

    private void iniciaEventos()
    {
        abreTelaVisualizacaoHospede();
        abreTelaVisualizacaoDependente();
        abreTelaVisualizacaoFuncionario();
        abreTelaVisualizacaoBordero();
    }

    private void abreTelaVisualizacaoHospede()
    {
        mniCadastroHospede.setOnAction((e) ->
        {
            HospedeTabelaView hospedeTableView = new HospedeTabelaView();
            iniciaVisualizacao(hospedeTableView);
        });
    }

    private void abreTelaVisualizacaoDependente()
    {
        mniCadastroDependente.setOnAction((e) ->
        {
            DependenteTabelaView dependenteTabelaView = new DependenteTabelaView();
            iniciaVisualizacao(dependenteTabelaView);
        });
    }

    private void abreTelaVisualizacaoFuncionario()
    {
        mniCadastroFuncionario.setOnAction((e) ->
        {
            FuncionarioTabelaView funcionarioTabelaView = new FuncionarioTabelaView();
            iniciaVisualizacao(funcionarioTabelaView);
        });
    }
    
    private void abreTelaVisualizacaoBordero()
    {
        mniCadastroBordero.setOnAction((e) -> 
        {
            BorderoTabelaView borderoTabelaView = new BorderoTabelaView();
            iniciaVisualizacao(borderoTabelaView);
        });
    }
    
    private void iniciaVisualizacao(TabelaView tabelaView)
    {
        root.getChildren().add(tabelaView.getWindow());
        tabelaView.setMainControllerRoot(root);
    }
}
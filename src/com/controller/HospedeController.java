package com.controller;

import com.controller.abstracts.PessoaController;
import com.model.dao.HospedeDAO;
import com.model.po.EnderecoHospede;
import com.model.po.Hospede;
import java.util.List;

public class HospedeController extends PessoaController<Hospede, EnderecoHospede>
{
    private HospedeDAO dao;

    @Override
    public void salvaOuAtualiza(Hospede hospede)
    {
        try
        {
            validador.valida(hospede);
            validadorEndereco.valida(hospede.getEndereco());
            if (!validador.ouveViolacaoDeRestricao())
            {
                try
                {
                    iniciaTransacao();
                    dao = new HospedeDAO(getEntityManager());
                    begin();
                    dao.salvaOuAtualiza(hospede);
                    commit();
                }
                catch (Exception e)
                {
                    executaRollback();
                }
                finally
                {
                    fechaConexao();
                }
            }
            else
            {
                configuraAlertaDeDadosPreenchidosIncorretamente();
                exibeAlertaComErros();
            }
        }
        catch (javax.el.PropertyNotFoundException e)
        {
            //Ignora pois é bug do próprio validator. Corrigido na versão alpha
        }
    }

    @Override
    public Hospede busca(Hospede hospede)
    {
        try
        {
            iniciaEntityManager();
            dao = new HospedeDAO(getEntityManager());
            return dao.pegaPorId(hospede.getId());
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public void deleta(Hospede hospede)
    {
        try
        {
            iniciaTransacao();
            dao = new HospedeDAO(getEntityManager());
            hospede = dao.pegaPorId(hospede.getId());
            EnderecoHospede endereco = getEntityManager().merge(hospede.getEndereco());
            hospede.setEndereco(endereco);
            begin();
            dao.deleta(hospede);
            commit();
        }
        catch (Exception e)
        {
            executaRollback();
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long getUltimoNumeroDeEntidade()
    {
        iniciaEntityManager();
        try
        {
            dao = new HospedeDAO(getEntityManager());
            Long numero = dao.getUltimoNumeroDeEntidade();
            return numero;
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Hospede> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        try
        {
            iniciaEntityManager();
            dao = new HospedeDAO(getEntityManager());
            return dao.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Hospede> buscaListaFiltrada(Hospede entidade, String[] propriedades)
    {
        try
        {
            iniciaEntityManager();
            dao = new HospedeDAO(getEntityManager());
            return dao.filtrar(entidade, propriedades);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long buscaTotalDeItens()
    {
        try
        {
            iniciaEntityManager();
            dao = new HospedeDAO(getEntityManager());
            return dao.buscaTotalDeItens();
        }
        finally
        {
            fechaConexao();
        }
    }
}
package com.controller;

import com.controller.abstracts.PessoaController;
import com.model.dao.FuncionarioDAO;
import com.model.po.EnderecoFuncionario;
import com.model.po.Funcionario;
import java.util.List;

public class FuncionarioController extends PessoaController<Funcionario, EnderecoFuncionario>
{
    private FuncionarioDAO dao;

    @Override
    public void salvaOuAtualiza(Funcionario funcionario)
    {
        try
        {
            validador.valida(funcionario);
            validadorEndereco.valida(funcionario.getEndereco());
            if (!validador.ouveViolacaoDeRestricao())
            {
                try
                {
                    iniciaTransacao();
                    dao = new FuncionarioDAO(getEntityManager());
                    begin();
                    dao.salvaOuAtualiza(funcionario);
                    commit();
                }
                catch (Exception e)
                {
                    executaRollback();
                }
                finally
                {
                    fechaConexao();
                }
            }
            else
            {
                configuraAlertaDeDadosPreenchidosIncorretamente();
                exibeAlertaComErros();
            }
        }
        catch (javax.el.PropertyNotFoundException e)
        {
            //Ignora pois é bug do próprio validator. Corrigido na versão alpha
        }
    }

    @Override
    public Funcionario busca(Funcionario funcionario)
    {
        try
        {
            iniciaEntityManager();
            dao = new FuncionarioDAO(getEntityManager());
            return dao.pegaPorId(funcionario.getId());
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public void deleta(Funcionario funcionario)
    {
        try
        {
            iniciaTransacao();
            dao = new FuncionarioDAO(getEntityManager());
            funcionario = dao.pegaPorId(funcionario.getId());
            EnderecoFuncionario endereco = getEntityManager().merge(funcionario.getEndereco());
            funcionario.setEndereco(endereco);
            begin();
            dao.deleta(funcionario);
            commit();
        }
        catch (Exception e)
        {
            executaRollback();
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long getUltimoNumeroDeEntidade()
    {
        iniciaEntityManager();
        try
        {
            dao = new FuncionarioDAO(getEntityManager());
            Long numero = dao.getUltimoNumeroDeEntidade();
            return numero;
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Funcionario> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        try
        {
            iniciaEntityManager();
            dao = new FuncionarioDAO(getEntityManager());
            return dao.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public List<Funcionario> buscaListaFiltrada(Funcionario entidade, String[] propriedades)
    {
        try
        {
            iniciaEntityManager();
            dao = new FuncionarioDAO(getEntityManager());
            return dao.filtrar(entidade, propriedades);
        }
        finally
        {
            fechaConexao();
        }
    }

    @Override
    public Long buscaTotalDeItens()
    {
        try
        {
            iniciaEntityManager();
            dao = new FuncionarioDAO(getEntityManager());
            return dao.buscaTotalDeItens();
        }
        finally
        {
            fechaConexao();
        }
    }
}
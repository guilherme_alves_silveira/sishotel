package com.view;

import com.controller.BorderoController;
import com.gui.importadores.interfaces.View;
import com.gui.template.TemplateView;
import com.gui.util.AlertaExceptionUtil;
import com.gui.util.FormattedTextFieldUtil;
import com.gui.util.LayoutManagerUtil;
import com.model.dao.GeralUtilDAO;
import com.model.po.Categoria;
import com.model.po.Despesa;
import com.model.po.Produto;
import com.model.po.Servico;
import com.model.po.abstracts.Bordero;
import com.model.po.enumerated.TipoConsumo;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Objects;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import jfxtras.labs.scene.control.BigDecimalField;
import jfxtras.scene.control.LocalTimePicker;

/**
 * CL = ChangeListener. AE = ActionEvent. MC = MouseClicked.
 *
 * @autor Guilherme A. Silveira
 */
public class BorderoView extends TemplateView implements View<Bordero>
{

    private Bordero bordero;
    private final BorderoController controller;
    /*BORDERO*/
    private TextField txtId;
    private TextField txtDescricao;
    private BigDecimalField txtDecValor;
    private ComboBox<TipoConsumo> cbTipoConsumo;
    private ComboBox<Categoria> cbCategoria;
    /*SERVIÇO*/
    private LocalTimePicker ltHoraInicio;
    private LocalTimePicker ltHoraFim;
    /*DESPESA*/
    private CheckBox chkDespesaCausadaPeloHospede;
    //************************************
    private Button btnSalvarAtualizar;
    private Button btnDeletar;
    private Button btnLimparTela;
    //************************************
    private HBox hbBotoesCRUD;
    //************************************
    private Label lbId;
    private Label lbDescricao;
    private Label lbValor;
    private Label lbTipoConsumo;
    private Label lbCategoria;
    private Label lbHoraInicio;
    private Label lbHoraFim;
    private Label lbTempoTotalServico;
    private Label lbTempoTotalServicoValor;
    private Label lbEstoque;
    private Label lbEstoqueValor;
    //************************************
    private GridPane gpDadosGeralLayout;
    private GridPane gpDadosDespesaLayout;
    private GridPane gpDadosServicoLayout;
    private GridPane gpDadosProdutoLayout;
    private TitledPane tpDespesa;
    private TitledPane tpServico;
    private TitledPane tpProduto;
    private BorderPane bpLayoutGeral;
    private Accordion adnBordero;
    //************************************
    private final ObservableList<Categoria> categorias = FXCollections.observableArrayList(GeralUtilDAO.listaCategorias());
    private final ObservableList<TipoConsumo> tipoConsumos = FXCollections.observableArrayList(TipoConsumo.values());

    public BorderoView()
    {
        this.controller = new BorderoController();
        inicia();
        getWindow().getContentPane().getChildren().add(bpLayoutGeral);
    }

    @Override
    protected void inicia()
    {
        getWindow().setMaxSize(850, 600);
        getWindow().setMinSize(850, 380);
        getWindow().setPrefSize(850, 380);
        getWindow().setTitle("Tela de Cadastro\\Alteração de Borderos.");
        iniciaBorderoLabels();
        iniciaBorderoFields();
        iniciaLayoutSuperior();
        iniciaDespesaLayout();
        iniciaServicoLayout();
        iniciaProdutoLayout();
        iniciaAccordionBordero();
        iniciaBotoesDeUtilizacaoGeral();
        iniciaLayoutGeral();
        iniciaEventos();
        iniciaComTpsDesabilitados();
        resetaTela();
    }

    private void iniciaBorderoLabels()
    {
        this.lbId = new Label("Código.:");
        this.lbDescricao = new Label("Descrição.:");
        this.lbValor = new Label("Preço.:");
        this.lbTipoConsumo = new Label("Tipo de Consumo.:");
        this.lbCategoria = new Label("Categoria.:");
        this.lbHoraInicio = new Label("Inicio.:");
        this.lbHoraFim = new Label("Fim.:");
        this.lbTempoTotalServico = new Label("Tempo total de serviço.:");
        this.lbTempoTotalServicoValor = new Label();
        this.lbEstoque = new Label("Estoque.:");
        this.lbEstoqueValor = new Label("0");
    }

    private void iniciaBorderoFields()
    {
        txtId = new TextField();
        txtDescricao = new TextField();
        txtDecValor = new BigDecimalField(BigDecimal.ZERO, BigDecimal.ONE, new DecimalFormat("#,##0.00"));
        cbTipoConsumo = new ComboBox<>();
        cbCategoria = new ComboBox<>();
        cbCategoria.setItems(categorias);
        cbTipoConsumo.setItems(tipoConsumos);
        ltHoraInicio = new LocalTimePicker(LocalTime.MIN);
        ltHoraFim = new LocalTimePicker(LocalTime.MAX);
        chkDespesaCausadaPeloHospede = new CheckBox("Prejuizo causado por terceiros?");
        FormattedTextFieldUtil.setNumericValidation(txtId, 10);
        FormattedTextFieldUtil.addTextLimiter(txtDescricao, 50);
    }

    private void iniciaLayoutSuperior()
    {
        this.gpDadosGeralLayout = new GridPane();
        LayoutManagerUtil.personalizaGriPane(gpDadosGeralLayout);
        gpDadosGeralLayout.setAlignment(Pos.CENTER);
        //Node, Coluna, Linha
        GridPane.setConstraints(lbId, 0, 0);
        GridPane.setConstraints(txtId, 1, 0);
        //************************************
        GridPane.setConstraints(lbDescricao, 2, 0);
        GridPane.setConstraints(txtDescricao, 3, 0);
        //************************************
        GridPane.setConstraints(lbValor, 0, 1);
        GridPane.setConstraints(txtDecValor, 1, 1);
        //************************************
        GridPane.setConstraints(lbTipoConsumo, 2, 1);
        GridPane.setConstraints(cbTipoConsumo, 3, 1);
        //************************************
        GridPane.setConstraints(lbCategoria, 0, 2);
        GridPane.setConstraints(cbCategoria, 1, 2);
        //************************************
        gpDadosGeralLayout.getChildren().addAll(lbId,
                                                lbDescricao,
                                                lbValor,
                                                lbTipoConsumo,
                                                lbCategoria,
                                                txtId,
                                                txtDescricao,
                                                txtDecValor,
                                                cbTipoConsumo,
                                                cbCategoria);
    }

    private void iniciaBotoesDeUtilizacaoGeral()
    {
        btnSalvarAtualizar = new Button("Salvar");
        btnDeletar = new Button("Deletar");
        btnLimparTela = new Button("Limpar Tela");
        hbBotoesCRUD = new HBox();
        hbBotoesCRUD.setSpacing(10);
        hbBotoesCRUD.setAlignment(Pos.CENTER);
        hbBotoesCRUD.getChildren().addAll(btnSalvarAtualizar,
                                          btnDeletar,
                                          btnLimparTela);
    }

    private void iniciaDespesaLayout()
    {
        //Node, Coluna, Linha
        GridPane.setConstraints(chkDespesaCausadaPeloHospede, 0, 0);
        //************************************
        this.gpDadosDespesaLayout = new GridPane();
        LayoutManagerUtil.personalizaGriPane(gpDadosDespesaLayout);
        gpDadosDespesaLayout.setAlignment(Pos.TOP_LEFT);
        gpDadosDespesaLayout.getChildren().add(chkDespesaCausadaPeloHospede);
        this.tpDespesa = new TitledPane("Dados de Despesa", gpDadosDespesaLayout);
        LayoutManagerUtil.SetMCEfeitoDeReduzirExpaendirTelaTitledPane(tpDespesa, getWindow());
    }

    private void iniciaServicoLayout()
    {
        //Node, Coluna, Linha
        GridPane.setConstraints(lbHoraInicio, 0, 0);
        GridPane.setConstraints(ltHoraInicio, 1, 0);
        //************************************
        GridPane.setConstraints(lbHoraFim, 2, 0);
        GridPane.setConstraints(ltHoraFim, 3, 0);
        //************************************
        GridPane.setConstraints(lbTempoTotalServico, 0, 1);
        GridPane.setConstraints(lbTempoTotalServicoValor, 1, 1);
        //************************************
        this.gpDadosServicoLayout = new GridPane();
        LayoutManagerUtil.personalizaGriPane(gpDadosServicoLayout);
        gpDadosServicoLayout.setAlignment(Pos.TOP_LEFT);
        gpDadosServicoLayout.getChildren().addAll(lbHoraInicio,
                                                  lbHoraFim,
                                                  lbTempoTotalServico,
                                                  lbTempoTotalServicoValor,
                                                  ltHoraInicio,
                                                  ltHoraFim);
        this.tpServico = new TitledPane("Dados de Serviço", gpDadosServicoLayout);
        LayoutManagerUtil.SetMCEfeitoDeReduzirExpaendirTelaTitledPane(tpServico, getWindow());
    }

    private void iniciaProdutoLayout()
    {
        //Node, Coluna, Linha
        GridPane.setConstraints(lbEstoque, 0, 1);
        GridPane.setConstraints(lbEstoqueValor, 1, 1);
        //*************************************
        this.gpDadosProdutoLayout = new GridPane();
        LayoutManagerUtil.personalizaGriPane(gpDadosProdutoLayout);
        gpDadosProdutoLayout.setAlignment(Pos.TOP_LEFT);
        gpDadosProdutoLayout.getChildren().addAll(lbEstoque,
                                                  lbEstoqueValor);
        this.tpProduto = new TitledPane("Dados de Produto", gpDadosProdutoLayout);
        LayoutManagerUtil.SetMCEfeitoDeReduzirExpaendirTelaTitledPane(tpProduto, getWindow());
    }

    private void iniciaAccordionBordero()
    {
        //Coloca identificadores para que sejam utilizados na função CLHabilitaSelecaoBordero()
        tpProduto.setId("PRODUTO");
        tpServico.setId("SERVICO");
        tpDespesa.setId("DESPESA");
        this.adnBordero = new Accordion(tpProduto, tpServico, tpDespesa);
        this.adnBordero.setPadding(new Insets(10));
    }

    private void iniciaLayoutGeral()
    {
        this.bpLayoutGeral = new BorderPane();
        bpLayoutGeral.setPadding(new Insets(10));
        bpLayoutGeral.setTop(gpDadosGeralLayout);
        bpLayoutGeral.setBottom(hbBotoesCRUD);
        bpLayoutGeral.setCenter(adnBordero);
    }

    /**
     * Inicia todos os eventos que serão executados nesta tela.
     */
    private void iniciaEventos()
    {
        CLHabilitaSelecaoBordero();
        AESalvaBordero();
        AEDeletaBordero();
        AELimparTela();
        CLBusca();
        vinculaSlidersServico();
        MCHabilitaBordero();
    }

    /**
     * Função que habilita os titledPanes por bordero. Se selecionado o bordero
     * produto, será habilidade o titledPane do produto, e será desabilitado de
     * todos os outros, o mesmo é válido para os outros.
     */
    private void CLHabilitaSelecaoBordero()
    {
        cbTipoConsumo.getSelectionModel().selectFirst();
        cbTipoConsumo.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends TipoConsumo> observable, TipoConsumo oldValue, TipoConsumo newValue) ->
                        {
                            if (!newValue.equals(oldValue))
                            {
                                habilitaSelecaoBordero(newValue);
                            }
                });
    }

    /**
     * Se clicado no brodero, a seleção já é habilitada com o tipo de consumo.
     */
    private void MCHabilitaBordero()
    {
        cbTipoConsumo.setOnMouseClicked((a) ->
        {
            habilitaSelecaoBordero(cbTipoConsumo.getValue());
        });
    }

    /**
     * Sobrecarga do método habilitaSelecaoBordero(TipoConsumo tipoConsumo).
     *
     * @param bordero
     */
    private void habilitaSelecaoBordero(Bordero bordero)
    {
        habilitaSelecaoBordero(bordero.getTipoConsumo());
    }

    /**
     * O titledPane é habilitado de acordo com o bordero\tipo de consumo
     * selecionado.
     *
     * @param tipoConsumo
     */
    private void habilitaSelecaoBordero(TipoConsumo tipoConsumo)
    {
        minimizaTodosTps();
        /**
         * Ativa o titledPane de acordo com o bordero\tipo de consumo.
         */
        adnBordero.getPanes().forEach((tp) ->
        {
            if (tipoConsumo.getDescricaoConsumo().equals(tp.getId()))
            {
                tp.setCollapsible(true);
                this.bordero = instanciaBorderoCorretoPorTpSelecionado(tp);
            }
            else
            {
                tp.setCollapsible(false);
            }
        });
    }

    /**
     * Primeiramente todos os titledPane são fechados, para que não fique mais
     * de um titledPane aberto ao mesmo tempo, se selecionado outro bordero.
     */
    private void minimizaTodosTps()
    {
        adnBordero.getPanes().forEach((tp) -> tp.setExpanded(false));
    }

    private void iniciaComTpsDesabilitados()
    {
        adnBordero.getPanes().forEach((tp) -> tp.setCollapsible(false));
    }

    /**
     * Ao realizar a alteração em qualquer um dos LocalTimePickers, é modificado
     * o Label lbTempoTotalServicoValor, alterando o seu texto.
     */
    private void vinculaSlidersServico()
    {
        ltHoraInicio.localTimeProperty().addListener((ObservableValue<? extends LocalTime> observable, LocalTime oldValue, LocalTime newValue) ->
        {
            calculaDiferencaHorasTrabalhadas();
        });

        ltHoraFim.localTimeProperty().addListener((ObservableValue<? extends LocalTime> observable, LocalTime oldValue, LocalTime newValue) ->
        {
            calculaDiferencaHorasTrabalhadas();
        });
    }

    /**
     * Calcula a diferença de horas entre o inicio e o fim de horas do serviço
     * prestado.
     */
    private void calculaDiferencaHorasTrabalhadas()
    {
        Long horasTrabalhadas;
        if (ltHoraInicio.getLocalTime().isBefore(ltHoraFim.getLocalTime()))
        {
            horasTrabalhadas = ChronoUnit.HOURS.between(ltHoraInicio.getLocalTime(), ltHoraFim.getLocalTime());
        }
        else // Caso contrário a hora de inicio é tratado como um dia anterior, para que o calculo seja feito de forma correta.
        {
            LocalDateTime tempHoraInicio = LocalDateTime.of(LocalDate.now().minusDays(1L), ltHoraInicio.getLocalTime());
            LocalDateTime tempHoraFim = LocalDateTime.of(LocalDate.now(), ltHoraFim.getLocalTime());
            horasTrabalhadas = ChronoUnit.HOURS.between(tempHoraInicio, tempHoraFim);
        }
        lbTempoTotalServicoValor.setText(horasTrabalhadas + "");
    }

    @Override
    public void setEntidade(Bordero bordero)
    {
        Objects.requireNonNull(bordero, "O bordero passadao não pode ser nulo.");
        this.bordero = controller.busca(bordero);
        this.getWindow().setPrefSize(850, 600);
        exibeDadosBordero();
    }

    /**
     * É retornado o tipo de bordero de acordo com o id do titledPane
     * selecionado.
     *
     * @param tp TitledPane
     *
     * @return Despesa, Servico ou Produto, todas subclasses de bordero.
     */
    private Bordero instanciaBorderoCorretoPorTpSelecionado(TitledPane tp)
    {
        switch (tp.getId())
        {
            case "DESPESA":
                return new Despesa();
            case "SERVICO":
                return new Servico();
            case "PRODUTO":
                return new Produto();
            default:
                throw new IllegalArgumentException("O bordero passado é inválido!");
        }
    }

    private void AESalvaBordero()
    {
        btnSalvarAtualizar.setOnAction((a) ->
        {
            if (bordero != null)
            {
                setDadosBordero();
                controller.salvaOuAtualiza(bordero);
                resetaTela();
            }
            else
            {
                Alert mensagem = new Alert(Alert.AlertType.INFORMATION);
                mensagem.setContentText("Selecione primeiramente o tipo de consumo.");
                mensagem.showAndWait();
            }
        });
    }

    private void AEDeletaBordero()
    {
        btnDeletar.setOnAction((a) ->
        {
            if (bordero != null && bordero.getId() != null)
            {
                bordero.setId(Long.parseLong(txtId.getText()));
                if (bordero.getId() > 0L)
                {
                    controller.deleta(bordero);
                    limparCampos();
                    resetaTela();
                }
            }
        });
    }

    /**
     * AE = ActionEvent.
     */
    protected final void AELimparTela()
    {
        btnLimparTela.setOnAction((e) ->
        {
            resetaTela();
        });
    }

    /**
     * CL = ChangeListener. Executa a busca do bordero, quando o foco é retirado
     * do txtId ou coloca o ultimo código da pessoa + 1.
     */
    protected void CLBusca()
    {
        txtId.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) ->
        {
            if (!newValue)
            {
                try
                {
                    Long id = 0L;
                    if (!txtId.getText().trim().equals(""))
                    {
                        id = Long.parseUnsignedLong(txtId.getText());
                    }

                    if (id > 0L)
                    {
                        this.bordero = controller.busca(id);
                        if (this.bordero != null)
                        {
                            armazenaIDAoLimparCampos();
                            exibeDadosBordero();
                        }
                        else
                        {
                            if (!id.equals(getUltimoNumeroDeEntidade()))
                            {
                                resetaTela();
                            }
                            else
                            {
                                armazenaIDAoLimparCampos();
                                TitledPane tp = getExpandedTitledPaneSelecionado();
                                this.bordero = instanciaBorderoCorretoPorTpSelecionado(tp);
                                this.bordero.setTipoConsumo(cbTipoConsumo.getValue());
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    new AlertaExceptionUtil(e).mostra();
                }
            }
        });
    }

    private TitledPane getExpandedTitledPaneSelecionado()
    {
        TitledPane tp = adnBordero.getPanes().get(cbTipoConsumo.getSelectionModel().getSelectedIndex());
        adnBordero.setExpandedPane(tp);
        return tp;
    }

    /**
     * Utilizado para armazenar o id quando os dados do txtId são apagados.
     */
    private void armazenaIDAoLimparCampos()
    {
        String tempID = txtId.getText();
        limparCampos();
        txtId.setText(tempID);
    }

    /**
     * Limpa os campos
     */
    private void limparCampos()
    {
        gpDadosGeralLayout.getChildren().forEach((no) -> limpaCampos(no));
        txtDecValor.setText("0.00");
        cbTipoConsumo.getSelectionModel().selectFirst();
    }

    @Override
    protected void resetaTela()
    {
        limparCampos();
        txtId.requestFocus();
        setUltimoNumero();
        this.bordero = null;
        minimizaTodosTps();
    }

    @Override
    public void permiteEdicao()
    {
        gpDadosGeralLayout.getChildren().forEach((no) -> habilitaCamposBotoes(no));
        hbBotoesCRUD.getChildren().forEach((no) -> habilitaCamposBotoes(no));
    }

    @Override
    public void permiteApenasVisualizacao()
    {
        gpDadosGeralLayout.getChildren().forEach((no) -> desabilitaCamposBotoes(no));
        hbBotoesCRUD.getChildren().forEach((no) -> desabilitaCamposBotoes(no));
    }

    @Override
    protected Long getUltimoNumeroDeEntidade()
    {
        return controller.getUltimoNumeroDeEntidade();
    }

    @Override
    protected final void setUltimoNumero()
    {
        try
        {
            Long ultimo = getUltimoNumeroDeEntidade();
            txtId.setText(String.valueOf(ultimo));
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    private void setDadosBordero()
    {
        if (bordero != null)
        {
            if (bordero.getTipoConsumo() == null)
            {
                bordero.setTipoConsumo(cbTipoConsumo.getValue());
            }
            habilitaSelecaoBordero(bordero);
            //*******************************
            String descricao = txtDescricao.getText();
            BigDecimal valor = txtDecValor.getNumber();
            Categoria categoria = cbCategoria.getValue();
            TipoConsumo tipo = cbTipoConsumo.getValue();
            //*******************************
            bordero.setDescricao(descricao);
            bordero.setValor(valor);
            bordero.setCategoria(categoria);
            bordero.setTipoConsumo(tipo);
            //*******************************
            if (bordero instanceof Servico)
            {
                Servico servico = (Servico) bordero;
                Calendar tempo = Calendar.getInstance();
                tempo.set(0, 0, 0, trataData(lbTempoTotalServicoValor.getText()), 0);
                servico.setTempo(tempo);
            }
            else if (bordero instanceof Despesa)
            {
                Despesa despesa = (Despesa) bordero;
                despesa.setPrejuizoCausadoPeloHospede(chkDespesaCausadaPeloHospede.isSelected());
            }
        }
    }

    private int trataData(String texto)
    {
        if (texto == null || "".equals(texto.trim()))
        {
            return 0;
        }
        else
        {
            return Integer.parseInt(texto);
        }
    }

    /**
     * Exibe os dados no bordero na tela, por tipo de bordero.
     */
    private void exibeDadosBordero()
    {
        if (bordero != null)
        {
            if (bordero.getId() != null)
            {
                txtId.setText(bordero.getId() + "");
            }
            txtDescricao.setText(bordero.getDescricao());
            txtDecValor.setNumber(bordero.getValor());
            cbCategoria.setValue(bordero.getCategoria());
            cbTipoConsumo.setValue(bordero.getTipoConsumo());
            getExpandedTitledPaneSelecionado();
            //*******************************
            if (bordero instanceof Produto)
            {
                Produto produto = (Produto) bordero;
                String estoque = produto.getEstoque() == null ? "0.0" : produto.getEstoque() + "";
                lbEstoqueValor.setText(String.valueOf(estoque));
            }
            else if (bordero instanceof Servico)
            {
                Servico servico = (Servico) bordero;
                ltHoraInicio.setLocalTime(LocalTime.MIN);
                ltHoraFim.setLocalTime(LocalTime.MIN);
            }
            else if (bordero instanceof Despesa)
            {
                Despesa despesa = (Despesa) bordero;
                chkDespesaCausadaPeloHospede.setSelected(despesa.isPrejuizoCausadoPeloHospede());
            }
        }
        else
        {
            throw new NullPointerException("O bordero não pode ser nulo");
        }
    }

    private Bordero instanciaBordero(TipoConsumo tipoConsumo, Long id)
    {
        switch (tipoConsumo)
        {
            case PRODUTO:
                return new Produto(id, tipoConsumo);
            case SERVICO:
                return new Servico(id, tipoConsumo);
            case DESPESA:
                return new Despesa(id, tipoConsumo);
            default:
                throw new IllegalArgumentException("Tipo de Consumo Inválido!");
        }
    }
}

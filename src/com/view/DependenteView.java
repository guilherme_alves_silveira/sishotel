package com.view;

import com.controller.DependenteController;
import com.gui.template.TemplataPessoa;
import com.gui.util.AlertaExceptionUtil;
import com.gui.util.LayoutManagerUtil;
import com.model.po.Dependente;
import com.model.po.EnderecoDependente;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;

public class DependenteView extends TemplataPessoa<Dependente, EnderecoDependente, DependenteController>
{
    private TextField txtGrauParentesco;
    //**********************************
    private Label lbGrauParentesco;
    //**********************************
    private GridPane gpDependenteCadastroLayout;
    private TitledPane tpDependenteCadastroLayout;

    public DependenteView()
    {
        inicia();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    @Override
    protected void inicia()
    {
        getWindow().setMaxSize(850, 600);
        getWindow().setMinSize(850, 380);
        getWindow().setPrefSize(850, 380);
        getWindow().setTitle("Tela de Cadastro\\Alteração de Dependentes.");
        //**************************************
        configuraImportador();
        iniciaLabelsDeCadastroPessoa();
        iniciaLabelsDeCadastroEndereco();
        iniciaBotoesDeUtilizacaoGeral();
        iniciaFieldsDeCadastroPessoa();
        iniciaLayoutDeCadastroPessoa();
        iniciaFieldDeCadastroEndereco();
        iniciaBotaoDeEndereco();
        iniciaLayoutDeCadastroEndereco();
        iniciaAccordionDeDadosExtras();
        iniciaLayoutGeral();
        iniciaCombosBoxes();
        iniciaDependenteLabels();
        iniciaDependenteFields();
        iniciaLayoutDeFuncionarioLabelsEFields();
        iniciaEventos();
        resetaTela();
    }

    @Override
    protected void resetaTela()
    {
        txtId.requestFocus();
        setUltimoNumero();
        this.pessoa = new Dependente();
        this.endereco = new EnderecoDependente();
    }

    private void iniciaEventos()
    {
        MCAjustrTelaAoMinOuMaxNoTitlePane();
        AEMudaCidadesDoComboAoMudarUF();
        AELimpaCidadesDoComboAoMudarUF();
        AEBuscaPais();
        AESalvaOuAtualiza();
        AELimparTela();
        AEDeleta();
        CLBusca();
    }

    private void iniciaDependenteLabels()
    {
        this.lbGrauParentesco = new Label("Grau de Parentesco.:");
    }

    private void iniciaDependenteFields()
    {
        this.txtGrauParentesco = new TextField();
    }

    @Override
    protected void MCAjustrTelaAoMinOuMaxNoTitlePane()
    {
        super.MCAjustrTelaAoMinOuMaxNoTitlePane();
        LayoutManagerUtil.SetMCEfeitoDeReduzirExpaendirTelaTitledPane(tpDependenteCadastroLayout, getWindow());
    }

    private void iniciaLayoutDeFuncionarioLabelsEFields()
    {
        this.gpDependenteCadastroLayout = new GridPane();
        //Node child, int columnIndex, int rowIndex
        GridPane.setConstraints(lbGrauParentesco, 0, 0);
        GridPane.setConstraints(txtGrauParentesco, 1, 0);
        //************************************************
        LayoutManagerUtil.personalizaGriPane(gpDependenteCadastroLayout);
        gpDependenteCadastroLayout.setAlignment(Pos.TOP_LEFT);
        gpDependenteCadastroLayout.getChildren().addAll(lbGrauParentesco,
                                                        txtGrauParentesco);
        this.tpDependenteCadastroLayout = new TitledPane("Informações Gerais do Dependente", gpDependenteCadastroLayout);
        getAdnDadosExtras().getPanes().add(tpDependenteCadastroLayout);
    }

    /* Métodos relacionados com o CRUD (FIM) */
    @Override
    protected Long getUltimoNumeroDeEntidade()
    {
        return controller.getUltimoNumeroDeEntidade();
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmPessoa()
    {
        this.pessoa = new Dependente();
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmPessoa(Dependente dependente)
    {
        this.pessoa = dependente;
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmEndereco()
    {
        if (this.endereco == null)
        {
            this.endereco = new EnderecoDependente();
        }
    }

    @Override
    protected void instanciaController()
    {
        this.controller = new DependenteController();
    }

    @Override
    protected void setDadosPessoaEnderecoNoObj()
    {
        super.setDadosPessoaEnderecoNoObj();
        try
        {
            Dependente dependente = (Dependente) pessoa;
            String grauParentesco = txtGrauParentesco.getText();
            dependente.setGrauParentesco(grauParentesco);
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    @Override
    protected void exibeDadosPessoaEndereco()
    {
        super.exibeDadosPessoaEndereco();
        if (this.pessoa != null)
        {
            try
            {
                Dependente dependente = (Dependente) pessoa;
                txtGrauParentesco.setText(dependente.getGrauParentesco());
            }
            catch (Exception e)
            {
                new AlertaExceptionUtil(e).mostra();
            }
        }
    }

    @Override
    protected Dependente retornaNovaInstanciaDeClasseFilha(Number ID)
    {
        return new Dependente(ID.longValue());
    }
}

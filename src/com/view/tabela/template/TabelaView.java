package com.view.tabela.template;

import com.gui.importadores.interfaces.View;
import com.gui.template.TemplatePaginator;
import com.model.base.Entidade;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public abstract class TabelaView<T extends Entidade, V extends View> extends TemplatePaginator<T>
{
    private V view;
    private BorderPane bpPosicionadorDePesquisaEBotoes;
    private HBox hbBotoesUtilitarios;
    protected Button btnEditar;
    protected Button btnCadastrarNovo;
    protected Button btnVisualizar;
    protected Button btnAtualizarTela;
    protected Pane mainRoot;

    public TabelaView(String titulo, String[] nomeDasColunas, String[] nomePropertys, Integer[] minWidth, Integer numCols)
    {
        super(titulo, nomeDasColunas, nomePropertys, minWidth, numCols);
    }

    protected void iniciaBotoesUtilitarios()
    {
        Image imgCadastro = new Image(getClass().getResourceAsStream("/com/imgs/icone_cadastro.png"));
        Image imgEdita = new Image(getClass().getResourceAsStream("/com/imgs/icone_edita.png"));
        Image imgVisualiza = new Image(getClass().getResourceAsStream("/com/imgs/icone_visualiza.png"));
        Image imgAtualiza = new Image(getClass().getResourceAsStream("/com/imgs/icone_atualiza.png"));
        this.hbBotoesUtilitarios = new HBox();
        this.btnCadastrarNovo = new Button("Novo Cadastro", new ImageView(imgCadastro));
        this.btnEditar = new Button("Editar", new ImageView(imgEdita));
        this.btnVisualizar = new Button("Visualizar", new ImageView(imgVisualiza));
        this.btnAtualizarTela = new Button("Atualizar (F5)", new ImageView(imgAtualiza));
        this.hbBotoesUtilitarios.setSpacing(10.0);
        hbBotoesUtilitarios.getChildren().addAll(btnCadastrarNovo,
                                                 btnEditar,
                                                 btnVisualizar,
                                                 btnAtualizarTela);
    }

    protected T getEntidadeSelecionada()
    {
        T entidade = this.getTbDados().getSelectionModel().getSelectedItem();
        return entidade;
    }

    protected void AEAtualiza()
    {
        btnAtualizarTela.setOnAction((e) ->
        {
            setNovosDados(buscaPaginadaDeDados(0, 5));
        });
    }

    protected void exibeMensagemDeAviso()
    {
        Alert mensagem = new Alert(Alert.AlertType.INFORMATION);
        mensagem.setHeaderText("Atenção!");
        mensagem.setContentText("Selecione um hospede por favor.");
    }

    protected void iniciaLayoutBotoesUtilitarios()
    {
        getBpLayoutGeral().getChildren().remove(getHbPesquisa());
        this.bpPosicionadorDePesquisaEBotoes = new BorderPane();
        BorderPane.setMargin(getHbPesquisa(), new Insets(5));
        BorderPane.setMargin(hbBotoesUtilitarios, new Insets(5));
        bpPosicionadorDePesquisaEBotoes.setRight(getHbPesquisa());
        bpPosicionadorDePesquisaEBotoes.setLeft(hbBotoesUtilitarios);
        getBpLayoutGeral().setBottom(bpPosicionadorDePesquisaEBotoes);
    }

    public void setMainControllerRoot(final Pane mainRoot)
    {
        Objects.requireNonNull(mainRoot, "O Pane do Main não pode ser nulo.");
        this.mainRoot = mainRoot;
    }

    protected void iniciaEventosBotoesUtilitarios()
    {
        AENovoCadastro();
        AEEditar();
        AEAtualiza();
        AEVisualiza();
    }

    protected void AEEditar()
    {
        btnEditar.setOnAction((e) ->
        {
            this.view = criaObjViewComReferenciaCorreta();
            T entidade = getEntidadeSelecionada();
            if (entidade != null)
            {
                view.setEntidade(entidade);
                view.permiteEdicao();
                mainRoot.getChildren().add(view.getWindow());
            }
            else
            {
                exibeMensagemDeAviso();
            }
        });
    }

    protected void AENovoCadastro()
    {
        btnCadastrarNovo.setOnAction((e) ->
        {
            this.view = criaObjViewComReferenciaCorreta();
            view.permiteEdicao();
            mainRoot.getChildren().add(view.getWindow());
        });
    }

    protected void AEVisualiza()
    {
        btnVisualizar.setOnAction((e) ->
        {
            this.view = criaObjViewComReferenciaCorreta();
            T entidade = getEntidadeSelecionada();
            if (entidade != null)
            {
                view.setEntidade(entidade);
                view.permiteApenasVisualizacao();
                mainRoot.getChildren().add(view.getWindow());
            }
            else
            {
                exibeMensagemDeAviso();
            }
        });
    }
    
    protected abstract V criaObjViewComReferenciaCorreta();
    
     /**
     *
     * @param criterios Os critérios são os valores colocados dentro da
     * entidade, pois o mesmo válida se as informações passadas, são compatíveis
     * com os atributos da entidade.
     *
     * @return A entidade com os critérios colocados.
     */
    protected abstract T getFiltro(String[] criterios);
}
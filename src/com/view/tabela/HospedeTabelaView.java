package com.view.tabela;

import com.controller.HospedeController;
import com.model.po.Hospede;
import com.view.HospedeView;
import com.view.tabela.template.TabelaView;
import java.util.List;

public class HospedeTabelaView extends TabelaView<Hospede, HospedeView>
{
    private static final String[] nomeDasColunas =
    {
        "ID", "Nome", "CPF", "Passaporte"
    };
    
    private static final String[] nomePropertys =
    {
        "id", "nome", "cpf", "passaporte"
    };
    
    private static final Integer[] minWidth =
    {
        20, 100, 120, 140
    };

    private HospedeController controller = new HospedeController();
    private HospedeView view;

    public HospedeTabelaView()
    {
        super("Pesquisa de Hospede", nomeDasColunas, nomePropertys, minWidth, nomeDasColunas.length);
        getWindow().setMaxSize(900, 700);
        getWindow().setMinSize(900, 700);
        getWindow().setPrefSize(900, 700);
        getWindow().setTitle("Tela de Pesquisa de Hospedes.");
        super.setBpLayoutGeralSize(850, 700);
        inicia();
    }

    @Override
    protected void inicia()
    {
        iniciaComboBoxDeQuantidade();
        iniciaCriacaoDeTableView();
        iniciaPagination();
        iniciaFieldEButtonGerais();
        iniciaLayoutGeral();
        iniciaBotoesUtilitarios();
        iniciaLayoutBotoesUtilitarios();
        iniciaEventos();
        iniciaEventosBotoesUtilitarios();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    @Override
    protected List<Hospede> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        return controller.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
    }

    @Override
    protected List<Hospede> buscaListaFiltrada(Hospede entidade, String... propriedades)
    {
        return controller.buscaListaFiltrada(entidade, propriedades);
    }

    @Override
    protected void AERealizaPesquisaComCriterios()
    {
        getBtnPesquisa().setOnAction((e) ->
        {
            String busca = getTxtPesquisa().getText().trim();
            if (!busca.equals(""))
            {
                String criterios[] = busca.split("\\s+");

                if (criterios.length > 0)
                {
                    Hospede entidade = getFiltro(criterios);
                    setNovosDados(buscaListaFiltrada(entidade, nomePropertys));
                }
            }
        });
    }

    @Override
    protected Long buscaTotalDeItens()
    {
        if (controller == null)
        {
            controller = new HospedeController();
        }
        return controller.buscaTotalDeItens();
    }

    @Override
    protected Hospede getFiltro(String[] criterios)
    {
        Hospede entidade = new Hospede();
        for (String criterio : criterios)
        {
            System.out.println(criterio);
            if (criterio.matches("[0-9]+"))
            {
                entidade.setId(Long.parseUnsignedLong(criterio));
            }
            else if (criterio.matches("[0-9.-]{14}"))
            {
                entidade.setCpf(criterio);
            }
            else if (criterio.matches("[0-9]{3,40}"))
            {
                entidade.setPassaporte(criterio);
            }
            else
            {
                entidade.setNome(criterio);
            }
        }
        return entidade;
    }

    @Override
    protected HospedeView criaObjViewComReferenciaCorreta()
    {
        return new HospedeView();
    }
}
package com.view.tabela;

import com.controller.DependenteController;
import com.model.po.Dependente;
import com.view.DependenteView;
import com.view.tabela.template.TabelaView;
import java.util.List;

public class DependenteTabelaView extends TabelaView<Dependente, DependenteView>
{
    private static final String[] nomeDasColunas =
    {
        "ID", "Nome", "CPF", "Passaporte"
    };
    private static final String[] nomePropertys =
    {
        "id", "nome", "cpf", "passaporte"
    };
    private static final Integer[] minWidth =
    {
        20, 100, 120, 140
    };

    private DependenteController controller = new DependenteController();
    private DependenteView view;

    public DependenteTabelaView()
    {
        super("Pesquisa de Dependentes de Hospedes", nomeDasColunas, nomePropertys, minWidth, nomeDasColunas.length);
        getWindow().setMaxSize(900, 700);
        getWindow().setMinSize(900, 700);
        getWindow().setPrefSize(900, 700);
        getWindow().setTitle("Tela de Pesquisa de Dependentes de Hospedes.");
        super.setBpLayoutGeralSize(850, 700);
        inicia();
    }

    @Override
    protected void inicia()
    {
        iniciaComboBoxDeQuantidade();
        iniciaCriacaoDeTableView();
        iniciaPagination();
        iniciaFieldEButtonGerais();
        iniciaLayoutGeral();
        iniciaBotoesUtilitarios();
        iniciaLayoutBotoesUtilitarios();
        iniciaEventos();
        iniciaEventosBotoesUtilitarios();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    @Override
    protected List<Dependente> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        return controller.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
    }

    @Override
    protected List<Dependente> buscaListaFiltrada(Dependente entidade, String... propriedades)
    {
        return controller.buscaListaFiltrada(entidade, propriedades);
    }

    @Override
    protected void AERealizaPesquisaComCriterios()
    {
        getBtnPesquisa().setOnAction((e) ->
        {
            String busca = getTxtPesquisa().getText().trim();
            if (!busca.equals(""))
            {
                String criterios[] = busca.split("\\s+");

                if (criterios.length > 0)
                {
                    Dependente dependente = getFiltro(criterios);
                    setNovosDados(buscaListaFiltrada(dependente, nomePropertys));
                }
            }
        });
    }

    @Override
    protected Long buscaTotalDeItens()
    {
        if (controller == null)
        {
            controller = new DependenteController();
        }
        return controller.buscaTotalDeItens();
    }

    @Override
    protected Dependente getFiltro(String[] criterios)
    {
        Dependente entidade = new Dependente();
        for (String criterio : criterios)
        {
            System.out.println(criterio);
            if (criterio.matches("[0-9]+"))
            {
                entidade.setId(Long.parseUnsignedLong(criterio));
            }
            else if (criterio.matches("[0-9.-]{14}"))
            {
                entidade.setCpf(criterio);
            }
            else if (criterio.matches("[0-9]{3,40}"))
            {
                entidade.setPassaporte(criterio);
            }
            else
            {
                entidade.setNome(criterio);
            }
        }
        return entidade;
    }

    @Override
    protected DependenteView criaObjViewComReferenciaCorreta()
    {
        return new DependenteView();
    }
}
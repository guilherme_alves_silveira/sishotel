package com.view.tabela;

import com.controller.BorderoController;
import com.model.po.Produto;
import com.model.po.abstracts.Bordero;
import com.view.BorderoView;
import com.view.tabela.template.TabelaView;
import java.util.List;

public class BorderoTabelaView extends TabelaView<Bordero, BorderoView>
{
    private static final String[] nomeDasColunas =
    {
        "ID", "Descrição", "Tipo de Consumo"
    };
    private static final String[] nomePropertys =
    {
        "id", "descricao", "descricaoTipoConsumo"
    };
    private static final Integer[] minWidth =
    {
        20, 140, 140
    };

    private BorderoController controller = new BorderoController();
    private BorderoView view;

    public BorderoTabelaView()
    {
        super("Pesquisa de Borderos", nomeDasColunas, nomePropertys, minWidth, nomeDasColunas.length);
        getWindow().setMaxSize(900, 700);
        getWindow().setMinSize(900, 700);
        getWindow().setPrefSize(900, 700);
        getWindow().setTitle("Tela de Pesquisa de Borderos.");
        super.setBpLayoutGeralSize(850, 700);
        inicia();
    }

    @Override
    protected void inicia()
    {
        iniciaComboBoxDeQuantidade();
        iniciaCriacaoDeTableView();
        iniciaPagination();
        iniciaFieldEButtonGerais();
        iniciaLayoutGeral();
        iniciaBotoesUtilitarios();
        iniciaLayoutBotoesUtilitarios();
        iniciaEventos();
        iniciaEventosBotoesUtilitarios();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    @Override
    protected List<Bordero> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        return controller.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
    }

    @Override
    protected List<Bordero> buscaListaFiltrada(Bordero entidade, String... propriedades)
    {
        return controller.buscaListaFiltrada(entidade, propriedades);
    }

    @Override
    protected void AERealizaPesquisaComCriterios()
    {
        getBtnPesquisa().setOnAction((e) ->
        {
            String busca = getTxtPesquisa().getText().trim();
            if (!busca.equals(""))
            {
                String criterios[] = busca.split("\\s+");

                if (criterios.length > 0)
                {
                    Bordero bordero = getFiltro(criterios);
                    setNovosDados(buscaListaFiltrada(bordero, nomePropertys));
                }
            }
        });
    }

    @Override
    protected Long buscaTotalDeItens()
    {
        if (controller == null)
        {
            controller = new BorderoController();
        }
        return controller.buscaTotalDeItens();
    }

    @Override
    protected Bordero getFiltro(String[] criterios)
    {
        Bordero entidade = new Produto();
        for (String criterio : criterios)
        {
            System.out.println(criterio);
            if (criterio.matches("[0-9]+"))
            {
                entidade.setId(Long.parseUnsignedLong(criterio));
            }
            else
            {
                entidade.setDescricao(criterio);
            }
        }
        return entidade;
    }

    @Override
    protected BorderoView criaObjViewComReferenciaCorreta()
    {
        return new BorderoView();
    }
}
package com.view.tabela;

import com.controller.FuncionarioController;
import com.model.po.Funcionario;
import com.view.FuncionarioView;
import com.view.tabela.template.TabelaView;
import java.util.List;

public class FuncionarioTabelaView extends TabelaView<Funcionario, FuncionarioView>
{
    private static final String[] nomeDasColunas =
    {
        "ID", "Nome", "CPF", "RG"
    };
    private static final String[] nomePropertys =
    {
        "id", "nome", "cpf", "rg"
    };
    private static final Integer[] minWidth =
    {
        20, 100, 120, 100
    };

    private FuncionarioController controller = new FuncionarioController();
    private FuncionarioView view;

    public FuncionarioTabelaView()
    {
        super("Pesquisa de Funcionários", nomeDasColunas, nomePropertys, minWidth, nomeDasColunas.length);
        getWindow().setMaxSize(900, 700);
        getWindow().setMinSize(900, 700);
        getWindow().setPrefSize(900, 700);
        getWindow().setTitle("Tela de Pesquisa de Funcionarios.");
        super.setBpLayoutGeralSize(850, 700);
        inicia();
    }

    @Override
    protected void inicia()
    {
        iniciaComboBoxDeQuantidade();
        iniciaCriacaoDeTableView();
        iniciaPagination();
        iniciaFieldEButtonGerais();
        iniciaLayoutGeral();
        iniciaBotoesUtilitarios();
        iniciaLayoutBotoesUtilitarios();
        iniciaEventos();
        iniciaEventosBotoesUtilitarios();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    @Override
    protected void AERealizaPesquisaComCriterios()
    {
        getBtnPesquisa().setOnAction((e) ->
        {
            String busca = getTxtPesquisa().getText().trim();
            if (!busca.equals(""))
            {
                String criterios[] = busca.split("\\s+");

                if (criterios.length > 0)
                {
                    Funcionario funcionario = getFiltro(criterios);
                    setNovosDados(buscaListaFiltrada(funcionario, nomePropertys));
                }
            }
        });
    }

    @Override
    protected List<Funcionario> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        return controller.buscaPaginadaDeDados(primeiroResultado, quantidadeDeResultados);
    }

    @Override
    protected List<Funcionario> buscaListaFiltrada(Funcionario entidade, String... propriedades)
    {
        return controller.buscaListaFiltrada(entidade, propriedades);
    }

    @Override
    protected Long buscaTotalDeItens()
    {
        if (controller == null)
        {
            controller = new FuncionarioController();
        }
        return controller.buscaTotalDeItens();
    }
    
    @Override
    protected Funcionario getFiltro(String[] criterios)
    {
        Funcionario entidade = new Funcionario();
        for (String criterio : criterios)
        {
            System.out.println(criterio);
            if (criterio.matches("[0-9]+"))
            {
                entidade.setId(Long.parseUnsignedLong(criterio));
            }
            else if (criterio.matches("[0-9.-]{14}"))
            {
                entidade.setCpf(criterio);
            }
            else if (criterio.matches("[0-9]{3,40}"))
            {
                entidade.setPassaporte(criterio);
            }
            else
            {
                entidade.setNome(criterio);
            }
        }
        return entidade;
    }

    @Override
    protected FuncionarioView criaObjViewComReferenciaCorreta()
    {
        return new FuncionarioView();
    }
}
package com.view.tabela;

import com.model.po.Reserva;
import com.view.ReservaView;
import com.view.tabela.template.TabelaView;
import java.util.List;

public class ReservaTabelaView  extends TabelaView<Reserva, ReservaView>
{
    public ReservaTabelaView(String titulo, String[] nomeDasColunas, String[] nomePropertys, Integer[] minWidth, Integer numCols)
    {
        super(titulo, nomeDasColunas, nomePropertys, minWidth, numCols);
    }
    
    @Override
    protected ReservaView criaObjViewComReferenciaCorreta()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Reserva getFiltro(String[] criterios)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void AERealizaPesquisaComCriterios()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected List<Reserva> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Long buscaTotalDeItens()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected List<Reserva> buscaListaFiltrada(Reserva entidade, String... propriedades)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void inicia()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
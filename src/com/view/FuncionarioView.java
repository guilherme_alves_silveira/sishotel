package com.view;

import com.controller.FuncionarioController;
import com.gui.template.TemplataPessoa;
import com.gui.util.AlertaExceptionUtil;
import com.gui.util.LayoutManagerUtil;
import com.model.dao.GeralUtilDAO;
import com.model.po.EnderecoFuncionario;
import com.model.po.Funcao;
import com.model.po.Funcionario;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalTime;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import jfxtras.labs.scene.control.BigDecimalField;
import jfxtras.scene.control.LocalTimePicker;

public class FuncionarioView extends TemplataPessoa<Funcionario, EnderecoFuncionario, FuncionarioController>
{
    private BigDecimalField txtDecSalario;
    private TextField txtCargo;
    private TextField txtLogin;
    private PasswordField passSenha;
    private LocalTimePicker ltHoraEntrada;
    private LocalTimePicker ltHoraSaida;
    private ComboBox<Funcao> cbFuncoes;
    //**********************************
    private Label lbSalario;
    private Label lbCargo;
    private Label lbLogin;
    private Label lbSenha;
    private Label lbHoraEntrada;
    private Label lbHoraSaida;
    private Label lbFuncoes;
    //**********************************
    private GridPane gpFuncionarioCadastroLayout;
    private TitledPane tpFuncionarioCadastroLayout;
    //**********************************
    private final ObservableList<Funcao> funcoes = FXCollections.observableArrayList();

    public FuncionarioView()
    {
        inicia();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    @Override
    protected void inicia()
    {
        getWindow().setMaxSize(850, 600);
        getWindow().setMinSize(850, 380);
        getWindow().setPrefSize(850, 380);
        getWindow().setTitle("Tela de Cadastro\\Alteração de Funcionarios.");
        //**************************************
        configuraImportador();
        iniciaLabelsDeCadastroPessoa();
        iniciaLabelsDeCadastroEndereco();
        iniciaFuncionarioLabels();
        iniciaBotoesDeUtilizacaoGeral();
        iniciaFieldsDeCadastroPessoa();
        iniciaLayoutDeCadastroPessoa();
        iniciaFieldDeCadastroEndereco();
        iniciaFuncionarioFields();
        iniciaBotaoDeEndereco();
        iniciaLayoutDeCadastroEndereco();
        iniciaAccordionDeDadosExtras();
        iniciaLayoutGeral();
        iniciaLayoutDeFuncionarioLabelsEFields();
        iniciaCombosBoxes();
        iniciaEventos();
        resetaTela();
    }

    @Override
    protected void resetaTela()
    {
        txtId.requestFocus();
        setUltimoNumero();
        this.pessoa = new Funcionario();
        this.endereco = new EnderecoFuncionario();
    }

    private void iniciaEventos()
    {
        MCAjustrTelaAoMinOuMaxNoTitlePane();
        AEMudaCidadesDoComboAoMudarUF();
        AELimpaCidadesDoComboAoMudarUF();
        AEBuscaPais();
        AELimparTela();
        AESalvaOuAtualiza();
        AEDeleta();
        CLBusca();
    }

    private void iniciaFuncionarioLabels()
    {
        this.lbSalario = new Label("Salário.:");
        this.lbCargo = new Label("Cargo.:");
        this.lbLogin = new Label("Login.:");
        this.lbSenha = new Label("Senha.:");
        this.lbHoraEntrada = new Label("Hora de Entrada.:");
        this.lbHoraSaida = new Label("Hora de Saída.:");
        this.lbFuncoes = new Label("Função.:");
    }

    private void iniciaFuncionarioFields()
    {
        //Valor inicial Zero. Incrementa de 1 em 1. Formado do campo vai ser 0,00 com valo de 0.00 caso esteja vázio.
        this.txtDecSalario = new BigDecimalField(BigDecimal.ZERO, BigDecimal.ONE, new DecimalFormat("#,##0.00"));
        this.txtCargo = new TextField();
        this.txtLogin = new TextField();
        this.passSenha = new PasswordField();
        this.ltHoraEntrada = new LocalTimePicker();
        this.ltHoraSaida = new LocalTimePicker();
        this.cbFuncoes = new ComboBox<>();
    }

    private void iniciaLayoutDeFuncionarioLabelsEFields()
    {
        //Node child, int columnIndex, int rowIndex
        GridPane.setConstraints(lbSalario, 0, 0);
        GridPane.setConstraints(txtDecSalario, 1, 0);
        //************************************************
        GridPane.setConstraints(lbCargo, 2, 0);
        GridPane.setConstraints(txtCargo, 3, 0);
        //************************************************
        GridPane.setConstraints(lbLogin, 0, 1);
        GridPane.setConstraints(txtLogin, 1, 1);
        //************************************************
        GridPane.setConstraints(lbSenha, 2, 1);
        GridPane.setConstraints(passSenha, 3, 1);
        //************************************************
        GridPane.setConstraints(lbHoraEntrada, 0, 2);
        GridPane.setConstraints(ltHoraEntrada, 1, 2);
        //************************************************
        GridPane.setConstraints(lbHoraSaida, 2, 2);
        GridPane.setConstraints(ltHoraSaida, 3, 2);
        //************************************************
        GridPane.setConstraints(lbFuncoes, 0, 3);
        GridPane.setConstraints(cbFuncoes, 1, 3);
        //************************************************
        this.gpFuncionarioCadastroLayout = new GridPane();
        LayoutManagerUtil.personalizaGriPane(gpFuncionarioCadastroLayout);
        gpFuncionarioCadastroLayout.setAlignment(Pos.CENTER);
        gpFuncionarioCadastroLayout.getChildren().addAll(txtDecSalario,
                                                         txtCargo,
                                                         txtLogin,
                                                         passSenha,
                                                         ltHoraEntrada,
                                                         ltHoraSaida,
                                                         cbFuncoes,
                                                         lbSalario,
                                                         lbCargo,
                                                         lbLogin,
                                                         lbSenha,
                                                         lbHoraEntrada,
                                                         lbHoraSaida,
                                                         lbFuncoes);
        this.tpFuncionarioCadastroLayout = new TitledPane("Informações Gerais do Funcionário", gpFuncionarioCadastroLayout);
        getAdnDadosExtras().getPanes().add(tpFuncionarioCadastroLayout);
    }

    @Override
    protected void MCAjustrTelaAoMinOuMaxNoTitlePane()
    {
        super.MCAjustrTelaAoMinOuMaxNoTitlePane();
        LayoutManagerUtil.SetMCEfeitoDeReduzirExpaendirTelaTitledPane(tpFuncionarioCadastroLayout, getWindow());
    }

    /* Métodos relacionados com o CRUD (FIM) */
    @Override
    protected Long getUltimoNumeroDeEntidade()
    {
        return controller.getUltimoNumeroDeEntidade();
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmPessoa()
    {
        this.pessoa = new Funcionario();
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmPessoa(Funcionario funcionario)
    {
        this.pessoa = funcionario;
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmEndereco()
    {
        if (this.endereco == null)
        {
            this.endereco = new EnderecoFuncionario();
        }
    }

    @Override
    protected void instanciaController()
    {
        this.controller = new FuncionarioController();
    }

    @Override
    protected void iniciaCombosBoxes()
    {
        super.iniciaCombosBoxes();
        funcoes.addAll(GeralUtilDAO.listaFuncoesDeFuncionario());
        cbFuncoes.setItems(funcoes);
    }

    @Override
    protected void setDadosPessoaEnderecoNoObj()
    {
        super.setDadosPessoaEnderecoNoObj();
        try
        {
            Funcionario funcionario = (Funcionario) pessoa;
            BigDecimal salario = txtDecSalario.getNumber();
            String cargo = txtCargo.getText();
            String login = txtLogin.getText();
            String senha = passSenha.getText();
            Integer horaEntrada = ltHoraEntrada.getLocalTime().getHour();
            Integer horaSaida = ltHoraSaida.getLocalTime().getHour();
            Funcao funcao = cbFuncoes.getValue();
            funcionario.setSalario(salario);
            funcionario.setCargo(cargo);
            funcionario.setLogin(login);
            funcionario.setSenha(senha);
            funcionario.setHoraEntrada(horaEntrada);
            funcionario.setHoraSaida(horaSaida);
            funcionario.setFuncao(funcao);
        }
        catch (Exception e)
        {
            new AlertaExceptionUtil(e).mostra();
        }
    }

    @Override
    protected void exibeDadosPessoaEndereco()
    {
        super.exibeDadosPessoaEndereco();
        if (this.pessoa != null)
        {
            try
            {
                Funcionario funcionario = (Funcionario) pessoa;
                txtDecSalario.setNumber(funcionario.getSalario());
                txtCargo.setText(funcionario.getCargo());
                txtLogin.setText(funcionario.getLogin());
                passSenha.setText(funcionario.getSenha());
                ltHoraEntrada.setLocalTime(LocalTime.of(funcionario.getHoraEntrada(), 0));
                ltHoraSaida.setLocalTime(LocalTime.of(funcionario.getHoraSaida(), 0));
                cbFuncoes.setValue(funcionario.getFuncao());
            }
            catch (Exception e)
            {
                new AlertaExceptionUtil(e).mostra();
            }
        }
    }

    @Override
    protected void limparCampos()
    {
        super.limparCampos();
        txtDecSalario.setNumber(BigDecimal.ZERO);
        ltHoraEntrada.withLocalTime(LocalTime.MIN);
        ltHoraSaida.withLocalTime(LocalTime.MIN);
        cbFuncoes.getSelectionModel().selectFirst();
    }

    @Override
    protected Funcionario retornaNovaInstanciaDeClasseFilha(Number ID)
    {
        return new Funcionario(ID.longValue());
    }
}
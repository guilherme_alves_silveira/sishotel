package com.view;

import com.controller.HospedeController;
import com.gui.template.TemplataPessoa;
import com.model.po.EnderecoHospede;
import com.model.po.Hospede;

public class HospedeView extends TemplataPessoa<Hospede, EnderecoHospede, HospedeController>
{
    public HospedeView()
    {
        inicia();
        getWindow().getContentPane().getChildren().add(getBpLayoutGeral());
    }

    @Override
    protected void inicia()
    {
        getWindow().setMaxSize(850, 600);
        getWindow().setMinSize(850, 320);
        getWindow().setPrefSize(850, 320);
        getWindow().setTitle("Tela de Cadastro\\Alteração de Hospedes.");
        //**************************************
        configuraImportador();
        iniciaLabelsDeCadastroPessoa();
        iniciaLabelsDeCadastroEndereco();
        iniciaBotoesDeUtilizacaoGeral();
        iniciaFieldsDeCadastroPessoa();
        iniciaLayoutDeCadastroPessoa();
        iniciaFieldDeCadastroEndereco();
        iniciaBotaoDeEndereco();
        iniciaLayoutDeCadastroEndereco();
        iniciaAccordionDeDadosExtras();
        iniciaLayoutGeral();
        iniciaCombosBoxes();
        iniciaEventos();
        resetaTela();
    }

    @Override
    protected void resetaTela()
    {
        txtId.requestFocus();
        setUltimoNumero();
        this.pessoa = new Hospede();
        this.endereco = new EnderecoHospede();
    }

    private void iniciaEventos()
    {
        MCAjustrTelaAoMinOuMaxNoTitlePane();
        AEMudaCidadesDoComboAoMudarUF();
        AELimpaCidadesDoComboAoMudarUF();
        AEBuscaPais();
        AELimparTela();
        AESalvaOuAtualiza();
        AEDeleta();
        CLBusca();
    }

    /* Métodos relacionados com o CRUD (FIM) */
    @Override
    protected Long getUltimoNumeroDeEntidade()
    {
        return controller.getUltimoNumeroDeEntidade();
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmPessoa()
    {
        this.pessoa = new Hospede();
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmPessoa(Hospede hospede)
    {
        this.pessoa = hospede;
    }

    @Override
    protected void colocaReferenciaDeObjHerdeiraEmEndereco()
    {
        if (this.endereco == null)
        {
            this.endereco = new EnderecoHospede();
        }
    }

    @Override
    protected void instanciaController()
    {
        this.controller = new HospedeController();
    }

    @Override
    protected Hospede retornaNovaInstanciaDeClasseFilha(Number ID)
    {
        return new Hospede(ID.longValue());
    }
}
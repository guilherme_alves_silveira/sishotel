package com.gui.importadores;

import com.view.BorderoView;
import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import jfxtras.labs.scene.layout.ScalableContentPane;

public class TestImportaPaisView extends Application
{
    private final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

    @Override
    public void start(Stage stage) throws Exception
    {
        ScalableContentPane scaledPane = new ScalableContentPane();
        BorderoView w1 = new BorderoView();
        scaledPane.setMinSize(800, 600);
        scaledPane.setMaxSize(dim.getWidth(), dim.getHeight());
        scaledPane.setAutoRescale(true);
        scaledPane.setAspectScale(true);
        Pane root = scaledPane.getContentPane();
        root.getChildren().addAll(w1.getWindow());
        Scene scene = new Scene(scaledPane);
        stage.setScene(scene);
        stage.setFullScreen(true);
        stage.setTitle("SisHotel - Gerenciamento de Hotéis");
        stage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
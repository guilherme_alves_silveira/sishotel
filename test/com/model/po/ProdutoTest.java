package com.model.po;

import com.util.ValidatorHelper;
import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class ProdutoTest
{
    private ValidatorHelper<Produto> validador;
    
    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
    }
    
    @Test
    public void testNaoDeveAceitarDescricaoVazia()
    {
        System.out.println("Não Deve Aceitar Descricao Vazia");
        Produto p = new Produto();
        p.setCategoria(new Categoria("Teste"));
        p.setValor(BigDecimal.ZERO);
        p.setDescricao("");
        validador.valida(p);
        assertTrue(validador.ouveViolacaoDeRestricao());
        p.setDescricao(null);
        validador.valida(p);
        assertTrue(validador.ouveViolacaoDeRestricao());
        p.setDescricao("      ");
        validador.valida(p);
        assertTrue(validador.ouveViolacaoDeRestricao());
    }

    @Test
    public void testNaoDeveAceitarValorNegativo()
    {
        System.out.println("Não Deve Aceitar Valor Negativo");
        Produto p = new Produto();
        p.setDescricao("Teste");
        p.setCategoria(new Categoria("Teste"));
        p.setValor(BigDecimal.ZERO);
        p.setValor(new BigDecimal(-10));
        validador.valida(p);
        assertTrue(validador.ouveViolacaoDeRestricao());
    }
    
    @Test
    public void testDeveAceitarValorPositivo()
    {
        System.out.println("Não Deve Aceitar Valor Negativo");
        Produto p = new Produto();
        p.setDescricao("Teste");
        p.setCategoria(new Categoria("Teste"));
        p.setValor(BigDecimal.ZERO);
        p.setValor(new BigDecimal(10));
        validador.valida(p);
        assertFalse(validador.ouveViolacaoDeRestricao());
    }
}

package com.model.po;

import com.util.ValidatorHelper;
import java.math.BigDecimal;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ServicoTest
{
    private ValidatorHelper<Servico> validador;

    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
    }

    @Test
    public void testNaoDeveAceitarTempoNulo()
    {
        System.out.println("Não Deve Aceitar Descricao Vazia");
        Servico s = new Servico();
        Calendar tempo = Calendar.getInstance();
        s.setTempo(tempo);
        s.setDescricao("Teste");
        s.setValor(BigDecimal.ZERO);
        validador.valida(s);
        assertFalse(validador.ouveViolacaoDeRestricao());
    }
}
package com.model.po;

import com.util.ValidatorHelper;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CategoriaTest
{
    private ValidatorHelper<Categoria> validador;
    
    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
    }
    
    @Test
    public void testNaoDeveAceitarDescricaoVazia()
    {
        System.out.println("Não Deve Aceitar Descricao Vazia");
        Categoria c = new Categoria();
        c.setDescricao("");
        validador.valida(c);
        assertTrue(validador.ouveViolacaoDeRestricao());
        c.setDescricao(null);
        validador.valida(c);
        assertTrue(validador.ouveViolacaoDeRestricao());
        c.setDescricao("      ");
        validador.valida(c);
        assertTrue(validador.ouveViolacaoDeRestricao());
    }
}
package com.model.po;

import com.model.vo.CalculadoraDiaria;
import com.util.ValidatorHelper;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class DiariaTest
{
    private ValidatorHelper<Diaria> validador;
    private Diaria diaria;
    private CalculadoraDiaria calculadora;
    private final DateFormat parser = new SimpleDateFormat("dd/MM/yyyy");

    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
        this.calculadora = new CalculadoraDiaria();
        this.diaria = new Diaria();
        Quarto quarto = new Quarto();
        quarto.setValor(new BigDecimal(150.0));
        diaria.setQuarto(quarto);
    }

    @Test
    public void calculoDeDiariaDeveTrazerResultadosExperadosComDiferencaDeUmDia()
    {
        Calendar dataEntrada = Calendar.getInstance();
        dataEntrada.set(Calendar.YEAR, 2014);
        dataEntrada.set(Calendar.MONTH, Calendar.MARCH);
        dataEntrada.set(Calendar.DAY_OF_MONTH, 1);
        dataEntrada.set(Calendar.HOUR_OF_DAY, 10);

        Calendar dataSaida = Calendar.getInstance();
        dataSaida.set(Calendar.YEAR, 2014);
        dataSaida.set(Calendar.MONTH, Calendar.MARCH);
        dataSaida.set(Calendar.DAY_OF_MONTH, 2);
        dataSaida.set(Calendar.HOUR_OF_DAY, 10);

        System.out.println(parser.format(dataEntrada.getTime()));
        System.out.println(parser.format(dataSaida.getTime()));

        diaria.setDataEntrada(dataEntrada);
        diaria.setDataSaida(dataSaida);

        calculadora.calcula(diaria);

        assertEquals(new BigDecimal("300.0"), diaria.getValor());
    }

    @Test
    public void calculoDeDiariaDeveTrazerResultadosExperadosComHoraDiferente()
    {
        Calendar dataEntrada = Calendar.getInstance();
        dataEntrada.set(Calendar.YEAR, 2014);
        dataEntrada.set(Calendar.MONTH, Calendar.MARCH);
        dataEntrada.set(Calendar.DAY_OF_MONTH, 1);
        dataEntrada.set(Calendar.HOUR_OF_DAY, 10);

        Calendar dataSaida = Calendar.getInstance();
        dataSaida.set(Calendar.YEAR, 2014);
        dataSaida.set(Calendar.MONTH, Calendar.MARCH);
        dataSaida.set(Calendar.DAY_OF_MONTH, 2);
        dataSaida.set(Calendar.HOUR_OF_DAY, 15);

        System.out.println(parser.format(dataEntrada.getTime()));
        System.out.println(parser.format(dataSaida.getTime()));

        diaria.setDataEntrada(dataEntrada);
        diaria.setDataSaida(dataSaida);

        calculadora.calcula(diaria);

        assertEquals(new BigDecimal("362.5"), diaria.getValor());
    }

    @Test(expected = IllegalStateException.class)
    public void calculoDeDiariaDeveLancarUmaExcecaoPorHorasDepoisSerAntes()
    {
        Calendar dataEntrada = Calendar.getInstance();
        dataEntrada.set(Calendar.YEAR, 2014);
        dataEntrada.set(Calendar.MONTH, Calendar.MARCH);
        dataEntrada.set(Calendar.DAY_OF_MONTH, 1);
        dataEntrada.set(Calendar.HOUR_OF_DAY, 10);

        Calendar dataSaida = Calendar.getInstance();
        dataSaida.set(Calendar.YEAR, 2013);
        dataSaida.set(Calendar.MONTH, Calendar.MARCH);
        dataSaida.set(Calendar.DAY_OF_MONTH, 2);
        dataSaida.set(Calendar.HOUR_OF_DAY, 15);

        System.out.println(parser.format(dataEntrada.getTime()));
        System.out.println(parser.format(dataSaida.getTime()));

        diaria.setDataEntrada(dataEntrada);
        diaria.setDataSaida(dataSaida);

        calculadora.calcula(diaria);
    }

    @Test
    public void calculoDeDiariaDeveTrazerResultadosComValoresPrecisos()
    {
        Quarto outro = new Quarto();
        outro.setValor(new BigDecimal("150.50063"));
        diaria.setQuarto(outro);
        Calendar dataEntrada = Calendar.getInstance();
        dataEntrada.set(Calendar.YEAR, 2014);
        dataEntrada.set(Calendar.MONTH, Calendar.MARCH);
        dataEntrada.set(Calendar.DAY_OF_MONTH, 1);
        dataEntrada.set(Calendar.HOUR_OF_DAY, 10);

        Calendar dataSaida = Calendar.getInstance();
        dataSaida.set(Calendar.YEAR, 2014);
        dataSaida.set(Calendar.MONTH, Calendar.MARCH);
        dataSaida.set(Calendar.DAY_OF_MONTH, 2);
        dataSaida.set(Calendar.HOUR_OF_DAY, 10);

        System.out.println(parser.format(dataEntrada.getTime()));
        System.out.println(parser.format(dataSaida.getTime()));

        diaria.setDataEntrada(dataEntrada);
        diaria.setDataSaida(dataSaida);

        calculadora.calcula(diaria);

        assertEquals(new BigDecimal("301.001260000008"), diaria.getValor());
    }

    @Test(expected = NullPointerException.class)
    public void naoDeveAceitarReservaNula()
    {
        Reserva reserva = null;
        this.diaria.setReserva(reserva);
    }

    @Test(expected = NullPointerException.class)
    public void naoDeveAceitarQuartoNuloAtravesDeReserva()
    {
        Reserva reserva = new Reserva(new Hospede(), null, Calendar.getInstance());
        this.diaria.setReserva(reserva);
    }

    @Test(expected = NullPointerException.class)
    public void naoDeveAceitarQuartoNulo()
    {
        this.diaria.setQuarto(null);
    }
}

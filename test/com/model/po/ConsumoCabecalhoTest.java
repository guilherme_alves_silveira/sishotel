package com.model.po;

import com.model.po.abstracts.Bordero;
import com.util.ValidatorHelper;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class ConsumoCabecalhoTest
{
    private ValidatorHelper<ConsumoCabecalho> validador;
    private ConsumoCabecalho cabecalho;
    private BigDecimal quantidade;
    private BigDecimal valor;
    private BigDecimal desconto;

    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
        this.cabecalho = new ConsumoCabecalho();
    }

    @Test
    public void deveFazerCalculoCorretamente()
    {
        for (int i = 0; i < 10; i++)
        {
            ConsumoUnitario consumo = new ConsumoUnitario();
            Bordero bordero = new Produto();
            bordero.setId(Long.parseUnsignedLong("" + (i + 1)));
            iniciaValores(new BigDecimal("5"), new BigDecimal("10"), new BigDecimal("0"));
            consumo.setQuantidade(quantidade)
                   .setDesconto(desconto)
                   .setValor(valor)
                   .setBordero(bordero);
            cabecalho.adicionaConsumos(consumo);
        }

        assertEquals(new BigDecimal("500"), cabecalho.getTotalGeral());
        assertEquals(new BigDecimal("500"), cabecalho.getTotalGeralComDesconto());
    }

    @Test
    public void deveFazerCalculoCorretamenteComDesconto()
    {
        for (int i = 0; i < 10; i++)
        {
            ConsumoUnitario consumo = new ConsumoUnitario();
            Bordero bordero = new Produto();
            bordero.setId(Long.parseUnsignedLong("" + (i + 1)));
            iniciaValores(new BigDecimal("5"), new BigDecimal("10"), new BigDecimal("1"));
            consumo.setQuantidade(quantidade)
                   .setDesconto(desconto)
                   .setValor(valor)
                   .setBordero(bordero);
            cabecalho.adicionaConsumos(consumo);
        }

        assertEquals(new BigDecimal("500"), cabecalho.getTotalGeral());
        assertEquals(new BigDecimal("490"), cabecalho.getTotalGeralComDesconto());
    }

    @Test
    public void deveFazerCalculoCorretamenteAORemoverItem()
    {
        for (int i = 0; i < 10; i++)
        {
            ConsumoUnitario consumo = new ConsumoUnitario();
            Bordero bordero = new Produto();
            bordero.setId(Long.parseUnsignedLong("" + (i + 1)));
            iniciaValores(new BigDecimal("5"), new BigDecimal("10"), new BigDecimal("0"));
            consumo.setQuantidade(quantidade)
                   .setDesconto(desconto)
                   .setValor(valor)
                   .setBordero(bordero);
            cabecalho.adicionaConsumos(consumo);
        }
        
        Bordero bordero = new Produto();
        bordero.setId(Long.parseUnsignedLong("1"));
        ConsumoUnitario consumo = new ConsumoUnitario();
        consumo.setValor(new BigDecimal(350))
               .setQuantidade(new BigDecimal(2))
               .setBordero(bordero);
        cabecalho.adicionaConsumos(consumo);

        assertEquals(new BigDecimal("1150"), cabecalho.getTotalGeral());
        assertEquals(new BigDecimal("1150"), cabecalho.getTotalGeralComDesconto());
    }
    
    @Test
    public void deveFazerCalculoCorretamenteAORemoverItemComDesconto()
    {
        for (int i = 0; i < 10; i++)
        {
            ConsumoUnitario consumo = new ConsumoUnitario();
            Bordero bordero = new Produto();
            bordero.setId(Long.parseUnsignedLong("" + (i + 1)));
            iniciaValores(new BigDecimal("5"), new BigDecimal("10"), new BigDecimal("0"));
            consumo.setQuantidade(quantidade)
                   .setDesconto(desconto)
                   .setValor(valor)
                   .setBordero(bordero);
            cabecalho.adicionaConsumos(consumo);
        }
        
        Bordero bordero = new Produto();
        bordero.setId(Long.parseUnsignedLong("1"));
        ConsumoUnitario consumo = new ConsumoUnitario();
        consumo.setValor(new BigDecimal(350))
               .setDesconto(new BigDecimal(100))
               .setQuantidade(new BigDecimal(2))
               .setBordero(bordero);
        cabecalho.adicionaConsumos(consumo);

        assertEquals(new BigDecimal("1150"), cabecalho.getTotalGeral());
        assertEquals(new BigDecimal("1050"), cabecalho.getTotalGeralComDesconto());
    }
    
    @Test
    public void deveFazerCalculoCorretamenteAORemoverItemComDescontoDiferente()
    {
        for (int i = 0; i < 10; i++)
        {
            ConsumoUnitario consumo = new ConsumoUnitario();
            Bordero bordero = new Produto();
            bordero.setId(Long.parseUnsignedLong("" + (i + 1)));
            iniciaValores(new BigDecimal("5"), new BigDecimal("10"), new BigDecimal("1"));
            consumo.setQuantidade(quantidade)
                   .setDesconto(desconto)
                   .setValor(valor)
                   .setBordero(bordero);
            cabecalho.adicionaConsumos(consumo);
        }
        
        Bordero bordero = new Produto();
        bordero.setId(Long.parseUnsignedLong("1"));
        ConsumoUnitario consumo = new ConsumoUnitario();
        consumo.setValor(new BigDecimal(350))
               .setDesconto(new BigDecimal(100))
               .setQuantidade(new BigDecimal(2))
               .setBordero(bordero);
        cabecalho.adicionaConsumos(consumo);

        assertEquals(new BigDecimal("1150"), cabecalho.getTotalGeral());
        assertEquals(new BigDecimal("1041"), cabecalho.getTotalGeralComDesconto());
    }
    
    private void iniciaValores(BigDecimal quantidade, BigDecimal valor, BigDecimal desconto)
    {
        this.quantidade = quantidade;
        this.valor = valor;
        this.desconto = desconto;
    }
}
package com.model.po;

import com.util.ValidatorHelper;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class ConsumoUnitarioTest
{
    private ValidatorHelper<ConsumoUnitario> validador;
    private ConsumoUnitario consumo;
    private BigDecimal quantidade;
    private BigDecimal valor;
    private BigDecimal desconto;

    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
        this.consumo = new ConsumoUnitario();
    }

    @Test
    public void deveCalcularCorretamentoNoConsumoUnitario()
    {
        this.quantidade = new BigDecimal(2.0);
        this.valor = new BigDecimal(10.0);

        consumo.setQuantidade(quantidade);
        consumo.setValor(valor);

        assertEquals(new BigDecimal("20"), consumo.getTotal());
        assertEquals(new BigDecimal("20"), consumo.getTotalComDesconto());
    }

    @Test
    public void deveCalcularCorretamentoNoConsumoUnitarioComDesconto()
    {
        this.quantidade = new BigDecimal(2.0);
        this.valor = new BigDecimal(10.0);
        this.desconto = new BigDecimal(5.0);

        consumo.setQuantidade(quantidade);
        consumo.setValor(valor);
        consumo.setDesconto(desconto);

        assertEquals(new BigDecimal("20"), consumo.getTotal());
        assertEquals(new BigDecimal("15"), consumo.getTotalComDesconto());
    }

    @Test
    public void deveCalcularCorretamentoNoConsumoUnitarioComThread()
    {
        this.quantidade = new BigDecimal(2.0);
        this.valor = new BigDecimal(10.0);
        this.desconto = new BigDecimal(5.0);

        consumo.setQuantidade(quantidade);
        consumo.setValor(valor);
        consumo.setDesconto(desconto);
        
        Thread thread = new Thread(consumo);
        thread.start();

        assertEquals(new BigDecimal("20"), consumo.getTotal());
        assertEquals(new BigDecimal("15"), consumo.getTotalComDesconto());
    }

    @Test
    public void deveCalcularCorretamentoNoConsumoUnitarioComDescontoImutavel()
    {
        this.quantidade = new BigDecimal(2.0);
        this.valor = new BigDecimal(10.0);
        this.desconto = new BigDecimal(5.0);

        consumo.setQuantidade(quantidade);
        consumo.setValor(valor);
        consumo.setDesconto(desconto);

        assertEquals(new BigDecimal("20"), consumo.getTotal());
        assertEquals(new BigDecimal("15"), consumo.getTotalComDesconto());

        consumo.setDesconto(BigDecimal.ZERO);
        assertEquals(new BigDecimal("20"), consumo.getTotal());
        assertEquals(new BigDecimal("20"), consumo.getTotalComDesconto());
    }

    @Test(expected = IllegalArgumentException.class)
    public void naoDeveAceitarValoresNegativosNaQuantidade()
    {
        this.quantidade = new BigDecimal(-1);
        this.valor = new BigDecimal(10.0);
        this.desconto = new BigDecimal(5.0);

        consumo.setQuantidade(quantidade);
        consumo.setValor(valor);
        consumo.setDesconto(desconto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void naoDeveAceitarValoresNegativosNoValor()
    {
        this.quantidade = new BigDecimal(10.0);
        this.valor = new BigDecimal(-1);
        this.desconto = new BigDecimal(5.0);

        consumo.setQuantidade(quantidade);
        consumo.setValor(valor);
        consumo.setDesconto(desconto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void naoDeveAceitarValoresNegativosNoDesconto()
    {
        this.quantidade = new BigDecimal(10.0);
        this.valor = new BigDecimal(10.0);
        this.desconto = new BigDecimal(-1);

        consumo.setQuantidade(quantidade);
        consumo.setValor(valor);
        consumo.setDesconto(desconto);
    }

    @Test
    public void identitficaBorderoECabecalhoNulo()
    {
        validador.valida(consumo);
        validador.imprimeMessages();
        assertEquals("2", String.valueOf(validador.quantidadeDeViolacoes()));
    }

}
package com.model.po;

import com.util.ValidatorHelper;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class HospedeTest
{
    private ValidatorHelper<Hospede> validador;

    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
    }

    @Test
    public void naoDeveAceitarValoresInvalidos()
    {
        try
        {
            Hospede h = new Hospede(new BigDecimal(-1));
            h.setCpf("21421412REGD");
            validador.valida(h);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        validador.imprimeMessages();
        assertEquals("4", String.valueOf(validador.quantidadeDeViolacoes()));
    }

    @Test
    public void deveAceitarCPFValido()
    {
        try
        {
            Hospede h = new Hospede(new BigDecimal(-1));
            h.setCpf("384.237.586-72");
            validador.valida(h);
        }
        catch (Exception e)
        {
            //Ignora
        }
        assertEquals("3", String.valueOf(validador.quantidadeDeViolacoes()));
    }
    
    /*
    @Test
    public void naoDeveAceitarMesmoDependenteMaisDeUmaVez()
    {
        Hospede h = new Hospede(new BigDecimal(-1));
        h.setCpf("");
        Dependente d1 = new Dependente();
        Dependente d2 = new Dependente();
        d1.setId(1L);
        d2.setId(1L);
        h.adicionaDependente(d1);
        h.adicionaDependente(d2);
        
        assertEquals("1", String.valueOf(h.getDependentes().size()));
    }

    @Test
    public void deveAceitarDependente()
    {
        Hospede h = new Hospede(new BigDecimal(-1));
        h.setCpf("");
        Dependente d1 = new Dependente();
        d1.setId(1L);
        h.adicionaDependente(d1);
        assertEquals("1", String.valueOf(h.getDependentes().size()));
    }

    @Test(expected = NullPointerException.class)
    public void naoDeveAceitarDependenteNulo()
    {
        Hospede h = new Hospede(new BigDecimal(-1));
        h.setCpf("");
        Dependente d1 = null;
        h.adicionaDependente(d1);
        validador.valida(h);
    }*/
}
package com.model.po;

import com.util.ValidatorHelper;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TipoPagamentoTest
{
    private ValidatorHelper<TipoPagamento> validador;

    @Before
    public void tearUp()
    {
        this.validador = new ValidatorHelper<>();
    }

    @Test
    public void testNaoDeveAceitarDescricaoVazia()
    {
        System.out.println("Não Deve Aceitar Descrição Vazia");
        TipoPagamento t = new TipoPagamento();
        t.setDescricao("");
        validador.valida(t);
        assertTrue(validador.ouveViolacaoDeRestricao());
        t.setDescricao(null);
        validador.valida(t);
        assertTrue(validador.ouveViolacaoDeRestricao());
        t.setDescricao("      ");
        validador.valida(t);
        assertTrue(validador.ouveViolacaoDeRestricao());
    }
}

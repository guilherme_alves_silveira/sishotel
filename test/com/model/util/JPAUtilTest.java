package com.model.util;

import com.util.JPAUtil;
import javax.persistence.EntityManager;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class JPAUtilTest
{
    @Test
    public void deveSeConectarComOBanco()
    {
        EntityManager em = null;
        try
        {
            em = JPAUtil.getEntityManager();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        assertNotNull(em);
    }
    
    @AfterClass
    public static void fechaFactory()
    {
        JPAUtil.closeEntityManagerFactory();
    }
}
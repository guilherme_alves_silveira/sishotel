package com.model.dao;

import com.model.po.abstracts.Bordero;
import com.util.JPAUtil;
import java.util.List;
import javax.persistence.EntityManager;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class TestBorderoDAO
{
    private EntityManager em;
    private BorderoDAO dao;
    
    @Before
    public void inicia()
    {
        this.em = JPAUtil.getEntityManager();
        this.dao = new BorderoDAO(em, Bordero.class);
    }
    
    @Test
    public void deveListaTodosBorderos()
    {
        List<Bordero> borderos = dao.buscaPaginadaDeDados(0, 5);
        Assert.assertEquals("TAMANHO DA LISTA", 4, borderos.size());
    }
    
    @Test
    public void deveBuscarTodosOsItens()
    {
        Long todosOsItens = dao.buscaTotalDeItens();
        Assert.assertEquals(4L, todosOsItens.longValue());
    }
    
    @Test
    public void devePegarUltimoItemMaisUm()
    {
        Long ultimoNumero = dao.getUltimoNumeroDeEntidade();
        Assert.assertEquals(6L, ultimoNumero.longValue());
    }
    
    @After
    public void fecha()
    {
        JPAUtil.closeEntityManager();
    }
    
    @AfterClass
    public static void fechaFactory()
    {
        JPAUtil.closeEntityManagerFactory();
    }
}